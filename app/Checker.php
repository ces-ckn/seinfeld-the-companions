<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Checker extends Model
{
    protected $table = 'checkers';
    protected $primaryKey = 'checkerId';

   	public function goal()
   	{
   		return $this->belongsTo('App\Goal', 'goalId', 'checkers');
   	}
}
