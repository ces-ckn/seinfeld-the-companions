<?php

namespace App\Console\Commands;

use DB;
use App\Frequency;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class RemindUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'remind:user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Set timezone from Vietnam city
        date_default_timezone_set("Asia/Ho_Chi_Minh");
        $currentDate = date("Y-m-d H:i:s");
        // Do you check the RemindMe ?
        function RemindMe($remind){
            if($remind){
                return true;
            }else{
                return false;
            }
        }
        //
        function Frequencies($reminder, $currentDate){
            switch ($reminder->type) {
                case 'daily':
                    switch ($reminder->repeat) {
                        case 'until':
                            // Set the startTime of goals like the "Y-m-d H-i-s"
                            $date = strtotime($reminder->start);
                            $startFormat = date('Y-m-d',$date);
                            $startTime = $reminder->startTime;
                            $startDate = "$startFormat $startTime"; // Important
                            $tmpStart = strtotime($startDate);
                            // Set the endTime of goals like the "Y-m-d H-i-s"
                            $endTime = $reminder->endTime;
                            $date = strtotime($reminder->end);
                            $endFormat = date('Y-m-d',$date);
                            $endDate = "$endFormat $endTime";// Important
                            $tmpEnd = strtotime($endDate);
                            //
                            $current = strtotime($currentDate);
                            $gapEndTime = round(($tmpEnd - $current) / 60);  
                            if($gapEndTime > 0){           
                                $gapStartTime = round(($tmpStart - $current) / 60);
                                // To remind users about theirs goals before a hour 
                                if((57 < $gapStartTime) && ($gapStartTime <= 60)){
                                    sendEmail($reminder);
                                }
                                // To remind users about theirs goals as it's started
                                if((2 < $gapStartTime) && ($gapStartTime <= 5)){
                                    sendEmail($reminder);
                                }
                            }
                            break;

                        case 'always':
                            // Set the startTime of goals like the "Y-m-d H-i-s"
                            $startTime = $reminder->startTime;
                            $subDate = substr($currentDate, 0, 10);
                            $startDate = "$subDate $startTime";// Important
                            $tmpStart = strtotime($startDate);
                            //     
                            $current = strtotime($currentDate);
                            $gapStartTime = round(($tmpStart - $current) / 60);
                            // To remind users about theirs goals before a hour 
                            if((57 < $gapStartTime) && ($gapStartTime <= 60)){
                                sendEmail($reminder);
                            }
                            // To remind users about theirs goals as it's started before 5 minutes
                            if((2 < $gapStartTime) && ($gapStartTime <= 5)){
                                sendEmail($reminder);
                            }
                            break;

                        default:
                            # code...
                            break;
                    }
                    break;

                case 'weekly':
                    switch ($reminder->repeat) {
                        case 'until':
                            $year = $reminder->freqYear;
                            // start date
                            $startWeek = $reminder->start - 1;
                            $time = strtotime("1 January $year", time());
                            $day = date('w', $time);
                            $time += ((7*$startWeek)+1-$day)*24*3600;
                            $startDate = date('Y-n-j', $time);
                            // end date
                            $endWeek = $reminder->end - 1;
                            $time = strtotime("1 January $year", time());
                            $day = date('w', $time);
                            $time += ((7*$endWeek)+1-$day)*24*3600;
                            $time += 6*24*3600;
                            $endDate = date('Y-n-j', $time);
                            //
                            $tmpEnd = strtotime($endDate);
                            $current = strtotime(substr($currentDate, 0, 10));
                            $gapEndTime = round(($tmpEnd - $current) / 60);  
                            if($gapEndTime > 0){
                                $tmpStart = strtotime($startDate);
                                $gapStartTime = round(($tmpStart - $current) / 60); 

                                if($gapStartTime <= 0){
                                    $remindDate = $reminder->remindDate;
                                    $tmpRemind = strtotime($remindDate);
                                    $gapRemindTime = round(($tmpRemind - $current) / 60);
                                    if($gapRemindTime == 0){
                                        sendEmail($reminder);

                                        $freq = Frequency::find($reminder->freqId);
                                        $freq->remindDate = date("Y-m-d", strtotime("+7 day", $tmpRemind));
                                        $freq->save();
                                    }
                                }
                            }
                            break;
                        
                        case 'always':
                            $year = $reminder->freqYear;
                            // start date
                            $startWeek = $reminder->start - 1;
                            $time = strtotime("1 January $year", time());
                            $day = date('w', $time);
                            $time += ((7*$startWeek)+1-$day)*24*3600;
                            $startDate = date('Y-n-j', $time);
                            //
                            $current = strtotime(substr($currentDate, 0, 10));
                            $tmpStart = strtotime($startDate);
                            $gapStartTime = round(($tmpStart - $current) / 60); 
                            if($gapStartTime <= 0){
                                $remindDate = $reminder->remindDate;
                                $tmpRemind = strtotime($remindDate);
                                $gapRemindTime = round(($tmpRemind - $current) / 60);
                                if($gapRemindTime == 0){
                                    sendEmail($reminder);

                                    $freq = Frequency::find($reminder->freqId);
                                    $freq->remindDate = date("Y-m-d", strtotime("+7 day", $tmpRemind));
                                    $freq->save();
                                }
                            }
                            break;

                        default:
                            # code...
                            break;
                    }
                    break;

                case 'monthly':
                    switch ($reminder->repeat) {
                        case 'until':
                            // Set the startTime of goals like the "Y-m-d"
                            $startDate = $reminder->remindDate;// Important
                            $tmpStart = strtotime($startDate);
                            // Set the endTime of goals like the "Y-m-d"
                            $endYear = $reminder->freqYear;// Year
                            $endMonth = $reminder->end;// Month
                            $subDate = substr($startDate, 8, 2);// Day
                            $endDate = "$endYear-$endMonth-$subDate";
                            $tmpEnd = strtotime($endDate);
                            //
                            $current = strtotime(substr($currentDate, 0, 10));
                            $gapEndTime = round(($tmpEnd - $current) / 60);

                            if($gapEndTime > 0){ 
                                $gapStartTime = round(($tmpStart - $current) / 60);  
                                if($gapStartTime == 0){
                                    sendEmail($reminder);

                                    $freq = Frequency::find($reminder->freqId);
                                    $freq->remindDate = date("Y-m-d", strtotime("+7 day", $tmpStart));
                                    $freq->save();
                                }
                            }
                            break;

                        case 'always':
                            // Set the startTime of goals like the "Y-m-d"
                            $startDate = $reminder->remindDate;// Important
                            $tmpStart = strtotime($startDate);
                            //
                            $current = strtotime(substr($currentDate, 0, 10));
                            $gapStartTime = round(($tmpStart - $current) / 60);  
                            if($gapStartTime == 0){
                                sendEmail($reminder);

                                $freq = Frequency::find($reminder->freqId);
                                $freq->remindDate = date("Y-m-d", strtotime("+7 day", $tmpStart));
                                $freq->save();
                            }
                            break;
                        
                        default:
                            # code...
                            break;
                    }
                    break;

                case 'yearly':
                    switch ($reminder->repeat) {
                        case 'until':
                            // Set the startTime of goals like the "Y-m-d"
                            $startDate = $reminder->remindDate;// Important
                            $tmpStart = strtotime($startDate);
                            // Set the endTime of goals like the "Y-m-d"
                            $endYear = $reminder->end;// Year
                            $subDate = substr($startDate, 5, 5);// Month-Day
                            $endDate = "$endYear-$subDate";// Month-Day
                            $tmpEnd = strtotime($endDate);
                            //
                            $current = strtotime(substr($currentDate, 0, 10));
                            $gapEndTime = round(($tmpEnd - $current) / 60);  

                            if($gapEndTime > 0){ 
                                $gapStartTime = round(($tmpStart - $current) / 60);  
                                if($gapStartTime == 0){
                                    sendEmail($reminder);

                                    $freq = Frequency::find($reminder->freqId);
                                    $freq->remindDate = date("Y-m-d", strtotime("+1 month", $tmpStart));
                                    $freq->save();
                                }
                            }
                            break;

                        case 'always':
                            // Set the startTime of goals like the "Y-m-d"
                            $startDate = $reminder->remindDate;// Important
                            $tmpStart = strtotime($startDate);
                            //
                            $current = strtotime(substr($currentDate, 0, 10));
                            $gapStartTime = round(($tmpStart - $current) / 60);  
                            if($gapStartTime == 0){
                                sendEmail($reminder);

                                $freq = Frequency::find($reminder->freqId);
                                $freq->remindDate = date("Y-m-d", strtotime("+1 month", $tmpStart));
                                $freq->save();
                            }
                            break;

                        default:
                            # code...
                            break;
                    }
                    break;

                default:
                    # code...
                    break;
            }
        }
        
        function sendEmail($reminder){
            $data['username'] = $reminder->username;
            $data['email'] = $reminder->email;
            $data['title'] = $reminder->title;

            Mail::send('emails.reminder', $data, function($message) use ($data){
                $message->to($data['email'], $data['username'])->subject('Your Reminder!');
            });
        }
        // Let's get started
        // Create a new Reminder table
        $reminders = DB::table('goals')
                    ->join('users', 'goals.userId', '=', 'users.userId')
                    ->join('frequencies', 'goals.goalId', '=', 'frequencies.goalId')
                    ->select('goals.title', 'goals.description', 'goals.startTime', 'goals.endTime', 'goals.remind', 'goals.repeat', 'frequencies.*', 'users.email', 'users.username')
                    ->get();
        // Scan all of records
        foreach ($reminders as $reminder) {
            if(RemindMe($reminder->remind)){
                //
                Frequencies($reminder, $currentDate);

            }
        }

        \Log::info('Let\'s get started');
        $this->info('Test has fired.');
    }
}
