<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Frequency extends Model
{
    protected $table = 'frequencies';
    protected $primaryKey = 'freqId';
    protected $fillable = [
    	'goalId',
    	'type', 
    	'freqYear', 
    	'start', 
    	'end',
        'remindDate'
    ];

    public function goal()
    {
    	return $this->belongsTo('App\Goal', 'goalId', 'freqId');
    }
}
