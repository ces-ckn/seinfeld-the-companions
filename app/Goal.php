<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Goal extends Model
{
    protected $table = 'goals';
    protected $primaryKey = 'goalId';
    protected $fillable = [
    	'userId',
    	'title', 
    	'description', 
    	'startTime', 
    	'endTime', 
    	'remind', 
    	'autocheck', 
    	'repeat', 
    	'goalStatus'
    ];

    // get the frequency
    public function frequency()
    {
    	return $this->hasOne('App\Frequency', 'goalId', 'freqId');
    }

    // get the user
    public function user()
    {
    	return $this->belongsTo('App\User', 'userId', 'goalId');
    }

    // get the checker
    public function checker()
    {
    	return $this->hasOne('App\Checker', 'goalId', 'checkerId');
    }
}
