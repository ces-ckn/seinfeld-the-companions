<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Checker;
use App\Goal;
use App\Frequency;
use App\User;
use DB;
use Response;
use Illuminate\Http\RedirectResponse;
use Input;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class GoalsController extends Controller
{
    public function getCreateGoal()
    {
        if(Auth::check()){
            return view('userPages.creategoal');
        } else {
            return redirect('/');
        }
    }

    public function postCreateGoal(Request $request)
    {
        date_default_timezone_set("Asia/Ho_Chi_Minh");
        $validator = $this->createGoalValidator($request->all());
        
        $goal = new Goal;
        $frequency = new Frequency;
        $user = new User;
        $user = Auth::user();
        $input = Input::all();

        $goal->user()->associate($user);

        if($validator->fails()){
        	$this->throwValidationException(
        		$request, $validator
        	);
        } else {
        	$goal->userId = $user->userId;
        	$goal->title = $input['goalTitle'];
	    	$goal->description = $input['goalDesc'];
	    	$goal->startTime = $input['goalStartTime'];
	    	$goal->endTime = $input['goalEndTime'];
	    	$goal->repeat = $input['repeat'];

	    	if(Input::has('remind-me')){
	    		$goal->remind = 1;
	    	} else {
	    		$goal->remind = 0;
	    	}

	    	if(Input::has('autocheck')){
	    		$goal->autocheck = 1;
	    	} else {
	    		$goal->autocheck = 0;
	    	}

	    	switch ($input['frequency']) {
	    		case 'daily':
	    			$frequency->type = 'daily';
	    			switch ($input['repeat']) {
	    				case 'always':
	    					$frequency->start = $input['goalStartDay'];
	    					break;
	    				case 'until':
	    					$frequency->start = $input['goalStartDay'];
	    					$frequency->end = $input['goalEndDay'];
	    					break;	
	    			}
	    			break;
	    		
	    		case 'weekly':
	    			$frequency->type = 'weekly';
                    $frequency->remindDate = date("Y-m-d");
	    			switch ($input['repeat']) {
	    				case 'always':
	    					$frequency->freqYear = $input['whichYearWeek'];
	    					$frequency->start = $input['startWeek'];
	    					break;	    				
	    				case 'until':
	    					$frequency->freqYear = $input['whichYearWeek'];
	    					$frequency->start = $input['startWeek'];
	    					$frequency->end = $input['endWeek'];
	    					break;	
	    			}
	    			break;

	    		case 'monthly':
	    			$frequency->type = 'monthly';
                    $frequency->remindDate = date("Y-m-d");
	    			switch ($input['repeat']) {
	    				case 'always':
	    					$frequency->freqYear = $input['whichYearMonth'];
	    					$frequency->start = $input['startMonth'];
	    					break;
	    				case 'until':
	    					$frequency->freqYear = $input['whichYearMonth'];
	    					$frequency->start = $input['startMonth'];
	    					$frequency->end = $input['endMonth'];
	    					break;	
	    			}
	    			break;

	    		case 'yearly':
	    			$frequency->type = 'yearly';
                    $frequency->remindDate = date("Y-m-d");
	    			switch ($input['repeat']) {
	    				case 'always':
	    					$frequency->start = $input['startYear'];
	    					break;
	    				case 'until':
	    					$frequency->start = $input['startYear'];
	    					$frequency->end = $input['endYear'];
	    					break;	
	    			}
	    			break;	
	    	}  	

	    	// save goal's record to database
	    	$goal->save();

	    	$frequency->goal()->associate($goal);
	    	$frequency->goalId = $goal->goalId;
	    	$frequency->save();

	    	return redirect('/usercp');
        }
    }

    public function getEditGoal(Request $request)
    {
        if(Auth::check()){
            return view('userPages.usercp');
        } else {
            return redirect('/');
        }
    }

    public function postEditGoal(Request $request)
    {
    	$goalid = $request->all();
        if (Input::get('editgoal')) {
            $goaldata = $this->editGoal($goalid['goalid']);
            return view('userPages.editgoal')->with('goaldata', $goaldata);
        } elseif (Input::get('delgoal')) {
            $this->delGoal($goalid['goalid']);
            return Redirect::back(); 
        }
    }

    public function editGoal($goalid)
    {
    	$goal_data = Goal::join('frequencies', 'goals.goalId', '=', 'frequencies.goalId')
    					->select(
    						'goals.goalId',
    						'goals.title', 
    						'goals.description', 
    						'goals.startTime', 
    						'goals.endTime',
    						'goals.remind', 
    						'goals.autocheck', 
    						'goals.repeat', 
    						'goals.goalStatus', 
    						'frequencies.type', 
    						'frequencies.freqYear', 
    						'frequencies.start', 
    						'frequencies.end'
    					)->where('goals.goalId', '=', $goalid)->get();
    	return $goal_data;
    }

    public function delGoal($goalid)
    {
        DB::table('goals')->where('goalId','=', $goalid)->delete();
    }

    public function getUpdateGoal()
    {
    	if(Auth::check()){
            return view('userPages.editgoal');
        } else {
            return redirect('/');
        }
    }

    public function postUpdateGoal(Request $request)
    {
    	$goaldata = $request->all();
    	
    	switch ($goaldata['goalFrequency']) {
    		case 'daily':
    			switch ($goaldata['goalRepeat']) {
    				case 'always':
    					$this->updateQuery($goaldata, 0, $goaldata['goalStartDay'], '');
    					break;
    				case 'until':
    					$this->updateQuery($goaldata, 0, $goaldata['goalStartDay'], $goaldata['goalEndDay']);
    					break;
    			}
    			break;
    		
    		case 'weekly':
    			switch ($goaldata['goalRepeat']) {
    				case 'always':
    					$this->updateQuery($goaldata, $goaldata['goalWhichYearWeek'], $goaldata['goalStartWeek'], '');
    					break;
    				case 'until':
    					$this->updateQuery($goaldata, $goaldata['goalWhichYearWeek'], $goaldata['goalStartWeek'], $goaldata['goalEndWeek']);
    					break;
    			}
    			break;

    		case 'monthly':
    			switch ($goaldata['goalRepeat']) {
    				case 'always':
    					$this->updateQuery($goaldata, $goaldata['goalWhichYearMonth'], $goaldata['goalStartMonth'], '');
    					break;
    				case 'until':
    					$this->updateQuery($goaldata, $goaldata['goalWhichYearMonth'], $goaldata['goalStartMonth'], $goaldata['goalEndMonth']);
    					break;
    			}
    			break;

    		case 'yearly':
    			switch ($goaldata['goalRepeat']) {
    				case 'always':
    					$this->updateQuery($goaldata, 0, $goaldata['goalStartYear'], '');
    					break;
    				case 'until':
    					$this->updateQuery($goaldata, 0, $goaldata['goalStartYear'], $goaldata['goalEndYear']);
    					break;
    			}
    			break;
    	}

    	return redirect('/usercp');
    }

    public function updateQuery($goaldata, $freqYear, $start, $end)
    {
    	if(Input::has('goalRemindme')){
    		$goalRemindme = 1;
    	} else {
    		$goalRemindme = 0;
    	}

    	if(Input::has('goalAutocheck')){
    		$goalAutocheck = 1;
    	} else {
    		$goalAutocheck = 0;
    	}

    	$goaldata_goalid = $goaldata['goalid'];
    	$goaldata_title = $goaldata['goalTitle'];
    	$goaldata_desc = $goaldata['goalDescription'];
    	$goaldata_startTime = $goaldata['goalStartTime'];
    	$goaldata_endTime = $goaldata['goalEndTime'];
    	$goaldata_repeat = $goaldata['goalRepeat'];
    	$goaldata_frequency = $goaldata['goalFrequency'];
        //$goaldata_goalStatus = "onProgress";

    	$updateQuery = "update goals, frequencies 
    			set goals.title= '$goaldata_title', 
    			goals.description='$goaldata_desc', 
    			goals.startTime='$goaldata_startTime', 
    			goals.endTime='$goaldata_endTime', 
    			goals.remind='$goalRemindme', 
    			goals.autocheck='$goalAutocheck', 
    			goals.repeat='$goaldata_repeat',
                goals.goalStatus = 'onProgress', 
    			frequencies.freqYear='$freqYear', 
    			frequencies.type='$goaldata_frequency', 
    			frequencies.start='$start', 
    			frequencies.end='$end' 
    			WHERE goals.goalId=frequencies.goalId 
    			AND goals.goalId='$goaldata_goalid'";
    	DB::update(DB::raw($updateQuery));
        // update 
        // delete checkers of goal
        DB::table('checkers')->where('goalId','=', $goaldata_goalid)->delete();
    }

    public function getGoalData()
    {
    	// using laravel query builder to retrieve data
        if(Auth::check()){
            $goals = Goal::all();
            $count = 0;
            $id = Auth::user()->userId;
            foreach($goals as $g){
                if($g->userId === $id){
                    $goal_data = $g->join('frequencies', 'goals.goalId', '=', 'frequencies.goalId')
                             ->select(
                            'goals.goalId',
                            'goals.title', 
                            'goals.description', 
                            'goals.startTime', 
                            'goals.endTime',
                            'goals.remind', 
                            'goals.autocheck', 
                            'goals.repeat', 
                            'goals.goalStatus', 
                            'frequencies.type', 
                            'frequencies.freqYear', 
                            'frequencies.start', 
                            'frequencies.end'
                            )->where('goals.userId','=',$id)->get();
                        
                    $show_tick = $g->join('checkers','goals.goalId','=','checkers.goalId')
                            ->join('frequencies', 'goals.goalId', '=', 'frequencies.goalId')
                            ->select(
                                'goals.goalId',
                                'checkers.items',
                                'checkers.tick',
                                'frequencies.type', 
                                'frequencies.start', 
                                'frequencies.end'    
                                )->where('goals.userId','=',$id)
                                ->orderBy('checkers.items', 'asc')
                                ->get();
                    $count++;
                }
            }
            if($count != 0){
                return view('userPages.usercp')->with([
                'goal_data' => json_encode($goal_data),
                'show_tick' => json_encode($show_tick),
                'count' => $count

                ]); 
            } else {
                return view('userPages.usercp')->with('count', $count);
            }
        }else{
            return redirect('/');
        }
    	
    }

    // public function getReport()
    // {
    //     // using laravel query builder to retrieve data
    //     if(Auth::check()){
            
    //     } else {
    //         return redirect('/');
    //     }
    // }

    // Validation in create goal form
    protected function createGoalValidator(array $data)
    {   
        return Validator::make($data, [
            'goalTitle' => 'required|min:6|max:100',
        ]);
    }
    public static function checkSuccessOrFail()
    {
        $goalId = (int) Input::get('id');
        $currentTime = Input::get('time');
        $status = Input::get('status');
        

        $tmp = Checker::where('goalId','=',$goalId)->where('items','=',$currentTime)->first();
        if(is_null($tmp)){
            $checker = new Checker;
            $checker->goalId = $goalId;
            $checker->items = $currentTime;
            $checker->tick = $status;
            $checker->save();
        }else{
            $tmp->tick = $status;
            $tmp->save();
        }
        // -----------------------------
        $goal = Goal::where('goalId','=',$goalId)->first();
        $frs = Frequency::where('goalId','=', $goalId)->first();
        $numOfSuccess = DB::table('checkers')->where('goalId','=',$goalId)->where('tick','=','success')->count();
        $numOfFail = DB::table('checkers')->where('goalId','=',$goalId)->where('tick','=','fail')->count();
        if($goal->repeat == 'until'){
            if($frs->type == 'daily'){
                $start = date_create($frs->start);
                $end = date_create($frs->end);
                $numOfDays = date_diff($start,$end)->format('%d') + 1;
                if($numOfFail == 0){
                    if($numOfSuccess == $numOfDays){
                    $goal->goalStatus = 'success';
                    }else{
                        $goal->goalStatus = 'onProgress';
                    }
                }else{
                    $goal->goalStatus = 'fail';
                }    
            }else{
                $start = (int) $frs->start;
                $end = (int) $frs->end;
                $num = ($end - $start) + 1;
                if($numOfFail == 0){
                    if($numOfSuccess == $num){
                        $goal->goalStatus = 'success';
                    }else{
                        $goal->goalStatus = 'onProgress';
                    }
                }else{
                    $goal->goalStatus = 'fail';
                }
            }
        }else{
            if($numOfFail==0){
                $goal->goalStatus = 'onProgress';
            }else{
                $goal->goalStatus = 'fail';
            }
        }
        $goal->save();
    }
}
