<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\Goal;
use App\User;
use App\Feedback;
use Input;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class PagesController extends Controller
{
    //
    public function index()
    {
        if(Auth::check()){
            return redirect('/usercp');
        }
        return view('homePages.index');
    }
    //
    public function contact(Request $request){
        $validator = $this->feedbackValidator($request->all());
        $fb = new Feedback;
        $input = Input::all();

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }else{
            $fb->subject = $input['subject'];
            $fb->email = $input['yourEmail'];
            $fb->message =  $input['message'];

            $fb->save();
            return Redirect::back();    
        }
    }

    public function profile() 
    {
        if(Auth::check()){
            return view('profile.profile');
        } else { 
            return redirect('/');   
        }
    }
    // Create a user table in manage users form
    public function admincp()
    {
        $num = 1;
        $users = DB::table('users')->get();
        if(Auth::check() && Auth::user()->role){
            return view('adminPages.admincp',['users'=>$users,'num'=>$num]);
        } else {
            return redirect('/');
        }
    }
    // 
    public function buttonAdmin()
    {
        //check which submit was clicked on
        if(Input::get('lock')) {
            $this->lockUser(); //if login then use this method
        }elseif(Input::get('delete')) {
            $this->deleteUser(); //if register then use this method
        }

        return Redirect::back();
    }    
    // Delete any users who checked in manage users form
    public function deleteUser(){
       $usersChecked = Input::get('users');

       foreach ($usersChecked as $userChecked) {
            DB::table('users')->where('userId', '=', $userChecked)->delete();
       }   
    }
    // Lock/Unlock any users who checked in manage users form
    public function lockUser(){
        $usersChecked = Input::get('users');

       foreach ($usersChecked as $userChecked) {
            $user = User::find($userChecked);
            if($user->status){
                $user->status = 0;
            }else{
                $user->status = 1;
            }
            $user->save();
       }  
    }
    public function userrp()
    {
        if(Auth::check()){
            $totalGoal = DB::table('goals')->where('userId','=',Auth::user()->userId)->count();
            $success = DB::table('goals')->where('goalStatus','=','success')->where('userId','=',Auth::user()->userId)->count();
            $fail = DB::table('goals')->where('goalStatus','=','fail')->where('userId','=',Auth::user()->userId)->count();
            $inProgress = DB::table('goals')->where('goalStatus','=','onProgress')->where('userId','=',Auth::user()->userId)->count();

            if($totalGoal == 0){
                $totalSuccess = 0;
            }else{
                $totalSuccess =  round((((float)$success/$totalGoal) * 100), 2);
            }

            // get longest process
            $goals = Goal::all();
            $count = 0;
            $id = Auth::user()->userId;
            foreach($goals as $g){
                if($g->userId === $id){
                    $goal_data = $g->select(
                            'goals.goalId',
                            'goals.title'
                            )->where('goals.userId','=',$id)
                            ->get();
                    $show_tick = $g->join('checkers','goals.goalId','=','checkers.goalId')
                            ->select(
                                'goals.goalId',
                                'checkers.items',
                                'checkers.tick'
                                )->where('goals.userId','=',$id)
                                ->orderBy('checkers.items', 'asc')
                                ->get();
                    $count++;
                }
            }
            if ($count != 0){
                return view('userPages.userrp')->with([
                    'totalGoal'=> $totalGoal,
                    'success'=> $success,
                    'fail'=> $fail,
                    'inProgress'=> $inProgress,
                    'totalSuccess'=> $totalSuccess,
                    'goal_data' => json_encode($goal_data),
                    'show_tick' => json_encode($show_tick),
                    'count' => $count
                ]); 
            } else {
                return view('userPages.userrp')->with([
                    'totalGoal'=> $totalGoal,
                    'success'=> $success,
                    'fail'=> $fail,
                    'inProgress'=> $inProgress,
                    'totalSuccess'=> $totalSuccess,
                    'count', $count
                ]);
            }


            // return view('userPages.userrp',['totalGoal'=>$totalGoal,
            //                                 'success'=>$success,
            //                                 'fail'=>$fail,
            //                                 'inProgress'=>$inProgress,
            //                                 'totalSuccess'=>$totalSuccess]);
        } else {
            return redirect('/');
        }
    }

    public function getFeedback()
    {
        $num = 1;
        $fbs = DB::table('feedbacks')->get();
        if(Auth::check() && Auth::user()->role){
            return view('adminPages.feedback',['fbs'=>$fbs,'num'=>$num]);
        } else {
            return redirect('/');
        }
    }

    public function postFeedBack(){
        $fbsChecked = Input::get('fbs');

       foreach ($fbsChecked as $fbChecked) {
            DB::table('feedbacks')->where('fbId', '=', $fbChecked)->delete();
       }  
       
       return Redirect::back();
    }

    // Validation in form-contact
    protected function feedbackValidator(array $data)
    {   
        return Validator::make($data, [
            'subject' => 'required|min:6|max:255',
            'yourEmail' => 'required|email|max:255',
            'message' => 'required|min:6',
        ]);
    }
}