<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Hash;
use Input;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{
    public function profileChange(Request $request)
    {
        $validator = $this->profileValidator($request->all());
        $user = new User;
        $user = Auth::user();
        $input = Input::all();
        //If oldpassword is in correct, which will create a new error
        if (!Hash::check($input['oldPassword'],$user->password)) {
            $validator->getMessageBag()->add('password', 'Your current password is incorrect.');
            return Redirect::back()->withErrors($validator)->withInput();
        }else if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }else{
            $user->username = $input['profileName'];
            $user->facebook = $input['profileFacebook'];
            $user->twitter =  $input['profileTwitter'];
            $user->ggplus = $input['profileGooglePlus'];
            if($input['password'] != "" && $input['password'] != null){
                $user->password = Hash::make($input['password']);
            }
            $user->save();
            return Redirect::back()->withInput()->with('success', 'Your profile has been changed successfully !!');    
        }

        Auth::login($this->create($request->all()));

        return redirect($this->redirectPath());
    }
    // Validation in form-profile
    protected function profileValidator(array $data)
    {   
        return Validator::make($data, [
            'password' => 'confirmed|min:6|max:255',
            'profileName' => 'min:6|max:50',
            'profileFacebook' => 'max:255',
            'profileTwitter' => 'max:255',
            'profileGooglePlus' => 'max:255',
            'oldPassword' => 'required|min:6|max:255',
        ]);
    }

}
