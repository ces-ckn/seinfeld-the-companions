<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', 'PagesController@index');
Route::post('/contact', 'PagesController@contact');

// Authentication routes...
Route::filter('banned',function(){
		if(Auth::check() && !Auth::user()->status){
			return redirect('/logout');
		}
});
Route::group(array('before' => 'banned'), function()
{       
	//use your routes 
	Route::get('/login', 'Auth\AuthController@getLogin');
	Route::post('/login', 'Auth\AuthController@postLogin');
	Route::get('/userrp','PagesController@userrp');
	Route::get('/profile', 'PagesController@profile');
	Route::post('/profile','UserController@profileChange');
	Route::get('/admincp','PagesController@admincp');
	Route::post('/admincp','PagesController@buttonAdmin');
	Route::get('/feedback','PagesController@getFeedback');
	Route::post('/feedback','PagesController@postFeedback');
	Route::get('/usercp','GoalsController@getGoalData');
	Route::get('/tick','GoalsController@checkSuccessOrFail');
	Route::controllers([
    	'password' => 'Auth\PasswordController',
	]);
	// Create goal routes
	Route::get('/creategoal','GoalsController@getCreateGoal');
	Route::post('/creategoal', 'GoalsController@postCreateGoal');

	// edit goal routes
	Route::get('/editgoal','GoalsController@getEditGoal');
	Route::post('/editgoal', 'GoalsController@postEditGoal');

	// update goal routes
	Route::get('/updategoal','GoalsController@getUpdateGoal');
	Route::post('/updategoal', 'GoalsController@postUpdateGoal');

});

Route::get('/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('/register', 'Auth\AuthController@getRegister');
Route::post('/register', 'Auth\AuthController@postRegister');





