<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('userId');
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->string('username');
            $table->boolean('role')->default(false);
            $table->boolean('status')->default(true);
            $table->string('facebook', 60);
            $table->string('twitter', 60);
            $table->string('ggplus', 60);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
