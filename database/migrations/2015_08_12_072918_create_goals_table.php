<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('goals', function (Blueprint $table) {
            $table->increments('goalId');
            $table->integer('userId')->unsigned();
            $table->foreign('userId')->references('userId')->on('users')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->string('title');
            $table->string('description');
            $table->time('startTime');  
            $table->time('endTime');
            $table->boolean('remind')->default(true);
            $table->boolean('autocheck')->default(true);
            $table->enum('repeat', ['always', 'until']);
            $table->enum('goalStatus', ['onProgress', 'success', 'fail']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('goals');
    }
}
