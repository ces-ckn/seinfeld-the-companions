<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFrequenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('frequencies', function (Blueprint $table) {
            $table->increments('freqId');
            $table->integer('goalId')->unsigned();
            $table->foreign('goalId')->references('goalId')->on('goals')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->enum('type', ['daily', 'weekly', 'monthly', 'yearly']);
            $table->integer('freqYear');
            $table->string('start');
            $table->string('end');
            $table->date('remindDate');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('frequencies');
    }
}
