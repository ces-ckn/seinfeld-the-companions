<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCheckersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checkers', function (Blueprint $table) {
            $table->increments('checkerId');
            $table->integer('goalId')->unsigned();
            $table->foreign('goalId')->references('goalId')->on('goals')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->string('items');
            $table->enum('tick', ['uncheck', 'success', 'fail']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('checkers');
    }
}
