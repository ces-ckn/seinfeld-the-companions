<?php
use Illuminate\Database\Seeder;
use App\User;
class UserTableSeeder extends Seeder
{

public function run()
{
    DB::table('users')->delete();
    User::create(array(
        'email'     => 'admin@admin.com',
        'password' => Hash::make('admin'),
        'name'    => 'Admin name',
        'birthday' => '12/05/1992',
        'gender'    => '0',
        'role'    => true,     
    ));
}

}