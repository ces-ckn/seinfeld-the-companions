// code to generate calendar's detail in month view
function generateMonthView(year, month){
	var url = window.location.pathname;
	url = url.substring(url.lastIndexOf('/') + 1);
	if (url === 'usercp'){
		var d = new Date();
		var prevMonth, nextMonth;
		var cal = new Calendar(0); // weeks starts on: 0 - Sunday, 1 - Monday
		var mdc = cal.monthDays(year, month); // get month
		var numOfWeek = mdc.length; // number of weeks in the current month
		countDay = 0;
		for (var i = 0; i < numOfWeek; i++){
	        for (var j = 0; j < 7; j++){
	        	dayId = document.getElementById('day' + countDay);        	
        		dayId.innerHTML = mdc[i][j];
        		countDay++;
	        }
		}
		switch(month){
			case 1:
				prevMonth = 12;
				nextMonth = 2;
				year -= 1;
				var numOfDayPrev = new Date(year, prevMonth, 0).getDate();
				var numOfDayNext = new Date(year, nextMonth, 0).getDate();
				var numOfZeroFirst = countZeroFirst(mdc[0]);
				var numOfZeroLast = countZeroLast(mdc, numOfWeek, numOfZeroFirst);
				displayDay(numOfDayPrev, numOfDayNext, numOfZeroFirst, numOfZeroLast);
				break;
			case 12:
				prevMonth = 11;
				nextMonth = 1;
				year += 1;
				var numOfDayPrev = new Date(year, prevMonth, 0).getDate();
				var numOfDayNext = new Date(year, nextMonth, 0).getDate();
				var numOfZeroFirst = countZeroFirst(mdc[0]);
				var numOfZeroLast = countZeroLast(mdc, numOfWeek, numOfZeroFirst);
				displayDay(numOfDayPrev, numOfDayNext, numOfZeroFirst, numOfZeroLast);
				break;
			default:
				prevMonth = month - 1;
				nextMonth = month + 1;
				var numOfDayPrev = new Date(year, prevMonth, 0).getDate();
				var numOfDayNext = new Date(year, nextMonth, 0).getDate();
				var numOfZeroFirst = countZeroFirst(mdc[0]);
				var numOfZeroLast = countZeroLast(mdc, numOfWeek, numOfZeroFirst);
				displayDay(numOfDayPrev, numOfDayNext, numOfZeroFirst, numOfZeroLast);
		}
	}
}

// function to count number of zeros available in the first array
function countZeroFirst(inputArray){
	var result = 0;
	for (var i = 0; i < 7; i++){
		if(inputArray[i] == 0){
			result++;
		}
	}
	return result;
}

// function to count number of zeros available in the last arrays
function countZeroLast(inputArray, numOfWeek, numOfZeroFirst){
	var result = 0;
	switch(numOfWeek){
		case 4:
			if (numOfZeroFirst === 0){
				result = 14;
			} else if (numOfZeroFirst === 7){
				result = 7;
			} else {
				result = 0;
			}
			break;
		case 5:
			for (var i = 0; i < 7; i++){
				if (inputArray[numOfWeek - 1][i] === 0) {
					result++;
				}
			}
			result += 7;
			break;
		default:	// numOfWeek = 6
			for (var j = 0; j < 7; j++){
				if ((inputArray[numOfWeek - 1][j] == 0) || (inputArray[numOfWeek - 2][j] == 0)) {
					result++;
				}
			}
	}
	return result;
}

// display day at the cells that doesn't belong to current month
function displayDay(numOfDayPrev, numOfDayNext, numOfZeroFirst, numOfZeroLast){
	var i = numOfDayPrev - numOfZeroFirst + 1;
	$('.cal-week .cal-day').removeClass('day-in-prev-month day-not-in-month day-in-next-month');
	for (var j = 0; j < numOfZeroFirst; j++){
		var dayId = document.getElementById('day' + j);
		$(dayId).parent().addClass('day-not-in-month');
		$(dayId).parent().addClass('day-in-prev-month');
		dayId.innerHTML = i;			
		i++;
	}
	var k = numOfZeroLast;
	for (var t = 41; t > 41 - numOfZeroLast; t--){
		var dayId = document.getElementById('day' + t);
		$(dayId).parent().addClass('day-not-in-month');
		$(dayId).parent().addClass('day-in-next-month');
		dayId.innerHTML = k;
		k--;
	}

	// remove checker on day not belong to current month
	$('.day-not-in-month').find('#checker-wrapper').remove();
}

// generate week view after page loaded
function generateWeekView(year){
	for (var i = 0; i < 8; i++){
		$('.week-view').append('<div class="cal-week-box" id="week-box' + i + '"></div>');
	}
	var countWeek = 1;
	for (var j = 0; j < 8; j++){
		for (var k = 0; k < 7; k++){
			$('#week-box' + j).append('<div class="cal-week-cell"><div id="week' + countWeek +'" class="cal-week-number"></div></div>');
			countWeek++;
		}
	}	
	for (var t = 1; t <= weeksInYear(year); t++){
		var tmp = dateRangeOfWeek(t, year);
		document.getElementById('week' + t).innerHTML = 'Week ' + t;
		$('#week' + t).after('<div>(' + tmp[0] + ' - ' + tmp[1] + ')</div>');
	}

	$('.cal-week-cell').on('click', function(){
        toggleGoal($(this).find('#checker'));
        var tmp = $(this).find('#checker-wrapper').siblings('.cal-week-number').text();
        var currentWeek = tmp.substring(tmp.indexOf(' ') + 1, tmp.length); 
        var goalID = parseInt($('#goalid').text());
		var status = "";
		if ($(this).find('#checker').hasClass("glyphicon-ok")){
			status = "success";
	    } else if ($(this).find('#checker').hasClass("glyphicon-remove")){
	        status = "fail";
	    } else if ($(this).find('#checker').hasClass("glyphicon-none")){
	        status = "uncheck";
	    }
	    if (currentWeek !== ''){
		    $.ajax({
	        	type: 'GET',
	        	url : "tick",
	        	data:{
	        		id: goalID,
	        		time: currentWeek,
	        		status: status
	        	},
	        });
	    }
	});
}

// function get date range of week in year
function dateRangeOfWeek(week, year) {
    var result = [];
    firstDay = new Date(year, 0, 1).getDay();
    var d = new Date("Jan 01, " + year + " 01:00:00");
    var w = d.getTime() - (3600000 * 24 * (firstDay - 1)) + 604800000 * (week - 1);
    var n1 = new Date(w);
    var n2 = new Date(w + 518400000);
    // result[0] = (n1.getMonth() + 1) + '/' + (n1.getDate() - 1) + '/' + n1.getFullYear();
    // result[1] = (n2.getMonth() + 1) + '/' + (n2.getDate() - 1) + '/' + n2.getFullYear();
    if (n1.getDate() < 10){
		result[0] = (n1.getMonth() + 1) + '/0' + (n1.getDate());
    } else {
    	result[0] = (n1.getMonth() + 1) + '/' + (n1.getDate());
    }

    if (n2.getDate() < 10){
		result[1] = (n2.getMonth() + 1) + '/0' + (n2.getDate());
    } else {
    	result[1] = (n2.getMonth() + 1) + '/' + (n2.getDate());
    }
    return result;
}

// generate year view after page loaded
function generateYearBlock(){
	beginYear = 2015;
	for (var i = 0; i < 8; i++){
		$('.year-view').append('<div class="cal-year-box" id="year-box' + i + '"></div>');
	}
	for (var j = 0; j < 8; j++){
		for (var k = 0; k < 7; k++){
			$('#year-box' + j).append('<div class="cal-year-cell">' + 
				'<div id="year' + beginYear +'" class="cal-year-number">' + beginYear + '</div></div>');
			beginYear++;
		}
	}
	// lastYear = beginYear;
	document.getElementById('title-month').innerHTML = $('.cal-year-cell').first().text() + ' - ' + $('.cal-year-cell').last().text();

	$('.cal-year-cell').on('click', function(){
        toggleGoal($(this).find('#checker'));
        var currentYear = $(this).find('#checker-wrapper').siblings('.cal-year-number').text();
        var goalID = parseInt($('#goalid').text());
		var status = "";
		if ($(this).find('#checker').hasClass("glyphicon-ok")){
			status = "success";
	    } else if ($(this).find('#checker').hasClass("glyphicon-remove")){
	        status = "fail";
	    } else if ($(this).find('#checker').hasClass("glyphicon-none")){
	        status = "uncheck";
	    }
	    if (currentYear !== ''){
		    $.ajax({
	        	type: 'GET',
	        	url : "tick",
	        	data:{
	        		id: goalID,
	        		time: currentYear,
	        		status: status
	        	},
	        });
	    }
	});
}

// count how many weeks in a year
function getWeekNumber(d) {
    // Copy date so don't modify original
    d = new Date(+d);
    d.setHours(0,0,0);
    // Set to nearest Thursday: current date + 4 - current day number
    // Make Sunday's day number 7
    d.setDate(d.getDate() + 4 - (d.getDay()||7));
    // Get first day of year
    var yearStart = new Date(d.getFullYear(),0,1);
    // Calculate full weeks to nearest Thursday
    var weekNo = Math.ceil(( ( (d - yearStart) / 86400000) + 1)/7)
    // Return array of year and week number
    return [d.getFullYear(), weekNo];
}
function weeksInYear(year) {
  	var d = new Date(year, 11, 31); // month from 0-11
  	var week = getWeekNumber(d)[1];
  	return week == 1? getWeekNumber(d.setDate(24))[1] : week;
}

// toggle success, fail or uncheck on calendar
function toggleGoal(checker){
	if(checker.is('.glyphicon-none')) {
        $(checker).removeClass('glyphicon glyphicon-none').addClass('glyphicon glyphicon-ok').css({
			'color': '#26C281',
			'font-size': '1.2em'
		});
		$(checker.parent()).css('border-color', '#26C281');
    } else if(checker.is('.glyphicon-ok')) {
        $(checker).removeClass('glyphicon glyphicon-ok').addClass('glyphicon glyphicon-remove').css({
			'color': 'red',
			'font-size': '1.2em'
		});
		$(checker.parent()).css('border-color', '#F15927');
    } else if(checker.is('.glyphicon-remove')) {
        $(checker).removeClass('glyphicon glyphicon-remove').addClass('glyphicon glyphicon-none');
    }
}