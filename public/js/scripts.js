$(document).ready(function(){
	var monthArr = ['January','February','March','April','May','June','July','August','September','October','November','December'];
	var weekArr = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
	var currentDay = new Date().getDay();
	var currentDate = new Date().getDate();

	// toggle signup button
	$(".btn-signup").click(function(){
		$("#form-login").slideUp("slow");
		$("#form-signup").slideToggle("slow");
		$('#noti').css("display", "none");
	});

	// toggle login button 
	$(".btn-login").click(function(){
		$("#form-signup").slideUp("slow");
		$("#form-login").slideToggle("slow");
		$('#noti').css("display", "none");
	});
	
	// script automatically highlight navigation bar
	$('.navbar a').click(function(){
	      $('.navbar a').removeClass('nav-current');
	      $(this).addClass('nav-current');
	});

	// highlight chosen menu
	$(function() {
	    $('.navbar a').each(function() {
	        if ($(this).prop('href') == window.location.href) {
	            $(this).addClass('nav-current');
	        }
	    });
	});
	
	// Check to see if the window is top if not then display button
	$(window).scroll(function(){
		if ($(this).scrollTop() > 100) {
			$('.scrollToTop').fadeIn();
		} else {
			$('.scrollToTop').fadeOut();
		}
	});
	
	// Click event to scroll to top
	$('.scrollToTop').click(function(){
		$('html, body').animate({scrollTop : 0}, 800);
		return false;
	});

	// click button to expand information
	$(".btn-down").click(function(){
		$(this).parent().find(".section-content").slideToggle(500);
		$(this).find('.glyphicon').toggleClass("rotate-180");
	});
	$('#aboutSection .section-trigger, #contactSection .section-trigger').on('click', function(){
		$(this).siblings('.section-content').slideToggle(500);
		$(this).siblings('.btn-down').find('.glyphicon').toggleClass("rotate-180");
	});

	// check for leap year
	function leapYear(year){
		return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
	}

	// generate components in usercp after page loaded
	var url = window.location.pathname;
	url = url.substring(url.lastIndexOf('/') + 1);
	if (url === 'usercp'){
		// show today
		var d = new Date();
		currentDate = d.getDate();
		currentMonth = d.getMonth() + 1;
		currentYear = d.getFullYear();
		if (currentDate < 10){
			currentDate = '0' + currentDate; 
		}
		if (currentMonth < 10){
			currentMonth = '0' + currentMonth;
		}
		$('#btn-today').text('Today: ' + currentMonth + '/' + currentDate + '/' + currentYear);

		// generate year view after page loaded
		generateYearBlock();

		// generate day view after page loaded
		for (var i = 0; i < 6; i++){
			$('.cal-month-box').append('<div class="cal-week" id="week' + i + '"></div');
		}
		var countDay = 0;
		for (var j = 0; j < 6; j++){
			for (var k = 0; k < 7; k++){
				$('#week' + j).append('<div class="cal-day">' + 
					'<div class="cal-date-number" id="day' + countDay + '"></div>' + 
					//'<div id="checker-wrapper"><span id="checker" class="glyphicon-none"></span></div>' +
					'</div>');
				countDay++;
			}
		}
		var date = $('#title-month').text();
		generateMonthView(new Date().getFullYear(), new Date().getMonth() + 1);

		// generate week view after page loaded
		generateWeekView(new Date().getFullYear());
	}

	// datetime picker in create goal
	var url = window.location.pathname;
	url = url.substring(url.lastIndexOf('/') + 1);
	if (url === 'creategoal'){
		// datepicker
		$('#startDay, #endDay').datepicker({
			changeYear: true,
			changeMonth: true
		});

		// default value
		$('#goalStartTime, #goalEndTime').val('00:00');
		$('#goalStartTime, #goalEndTime').timepicker({
			timeFormat: 'HH:mm',
			currentText: 'now'
		});
	}

	var url = window.location.pathname;
	url = url.substring(url.lastIndexOf('/') + 1);
	if (url === 'editgoal'){
		// datepicker
		$('#startDay, #endDay').datepicker({
			changeYear: true,
			changeMonth: true
		});

		// default value
		$('#goalStartTime, #goalEndTime').timepicker({
			timeFormat: 'HH:mm',
			currentText: 'now'
		});
	}
	
	// function to check success, fail... in daily and monthly view
	var url = window.location.pathname; 
	url = url.substring(url.lastIndexOf('/') + 1);
	if (url === 'usercp'){
		$('.cal-month').on('click', function(){
	        toggleGoal($(this).find('#checker'));
	        var tmp = $(this).find('#checker-wrapper').siblings('.cal-month-name').text();
	        var currentMonth = monthArr.indexOf(tmp) + 1; 
			var status = "";
			var goalID = parseInt($('#goalid').text());
			if ($(this).find('#checker').hasClass("glyphicon-ok")){
					status = "success";
	        } else if ($(this).find('#checker').hasClass("glyphicon-remove")){
	        		status = "fail";
	        } else if ($(this).find('#checker').hasClass("glyphicon-none")){
	        		status = "uncheck";
	        }
	        if (currentMonth !== 0){
		        $.ajax({
		        	type: 'GET',
		        	url : "tick",
		        	data:{
		        		id: goalID,
		        		time: currentMonth,
		        		status: status,
		        	},
		        });
		    }
		});

		$('.cal-day').on('click', function(){
    		toggleGoal($(this).find('#checker'));
    		var tmp = $('#title-month').text();
			var currentMonth = monthArr.indexOf(tmp.substring(0, tmp.indexOf(' '))) + 1;
			if (parseInt(currentMonth) < 10){
				currentMonth = '0' + currentMonth;
			}
			var currentYear = tmp.substring(tmp.indexOf(' ') + 1, tmp.length);
			tmp = parseInt($(this).find('#checker-wrapper').siblings('.cal-date-number').text());
			if (tmp < 10){
				var currentDate = currentMonth + "/0" + $(this).find('#checker-wrapper').siblings('.cal-date-number').text() + "/" + currentYear;
			} else {
				var currentDate = currentMonth + "/" + $(this).find('#checker-wrapper').siblings('.cal-date-number').text() + "/" + currentYear;
			}
			var goalID = parseInt($('#goalid').text());
			var status = "";
			if ($(this).find('#checker').hasClass("glyphicon-ok")){
				status = "success";
    		} else if ($(this).find('#checker').hasClass("glyphicon-remove")){
    			status = "fail";
    		} else if ($(this).find('#checker').hasClass("glyphicon-none")){
    			status = "uncheck";
    		}

    		// push data into database
    		if (currentDate.length === 10){
		        $.ajax({
		        	type: 'GET',
		        	url : "tick",
		        	data: {
		        		id: goalID,
		        		time: currentDate,
		        		status: status
		        	},
		        });
		    }
		});

		$('#goal-detail #del-goal').on('click', function(){
			document.getElementById('modal-content').innerHTML = 'Are you sure you want to delete this goal: ' + $('#goalList option:selected').text() + '?';
		});
	}

	// click button to view next or previous month in day view
	var url = window.location.pathname;
	url = url.substring(url.lastIndexOf('/') + 1);
	if (url === 'usercp'){
		var currentYear = new Date().getFullYear();
		var currentMonth = new Date().getMonth() + 1;
		document.getElementById('title-month').innerHTML = monthArr[currentMonth - 1] + ' ' + currentYear;
		
		// click button to view next months
		$('.cal-month-box, .cal-header').on('click', '#btn-nextMonth-dayView, .day-in-next-month', function(){
			$('.month-view-today').removeClass('month-view-today');
			var tmp = $('#title-month').text();
			currentMonth = monthArr.indexOf(tmp.substring(0, tmp.length - 5)) + 1;
			if (currentMonth === 12){
				currentYear = parseInt(tmp.substring(tmp.length - 4, tmp.length));
				currentYear++;
				currentMonth = 0;
				document.getElementById('title-month').innerHTML = monthArr[currentMonth] + ' ' + currentYear;
				generateMonthView(currentYear, ++currentMonth);
				
			} else {
				currentYear = parseInt(tmp.substring(tmp.length - 4, tmp.length));
				document.getElementById('title-month').innerHTML = monthArr[currentMonth] + ' ' + currentYear;
				generateMonthView(currentYear, ++currentMonth);
			}
		});

		// click button to view previous months
		$('.cal-month-box, .cal-header').on('click', '#btn-prevMonth-dayView, .day-in-prev-month', function(){
			$('.month-view-today').removeClass('month-view-today');
			var tmp = $('#title-month').text();
			currentYear = parseInt(tmp.substring(tmp.length - 4, tmp.length));
			currentMonth = monthArr.indexOf(tmp.substring(0, tmp.length - 5)) + 1;
			if(currentMonth > 1 || currentYear > 2015){
				if (currentMonth === 1){
					currentYear--;
					currentMonth = 13;
					document.getElementById('title-month').innerHTML = monthArr[currentMonth - 2] + ' ' + currentYear;
					generateMonthView(currentYear, --currentMonth);
				} else {
					document.getElementById('title-month').innerHTML = monthArr[currentMonth - 2] + ' ' + currentYear;
					generateMonthView(currentYear, --currentMonth);
				}
			}
		});

		// click to view current day
		// $('#btn-today').click(function(){
		// 	currentDate = new Date().getDate();
		// 	generateMonthView(new Date().getFullYear(), new Date().getMonth() + 1);
		// 	document.getElementById('title-month').innerHTML = monthArr[new Date().getMonth()] + ' ' + new Date().getFullYear();
		// 	for(var i = 0; i < 42; i++){
		// 		var tmp = $('#day' + i).text();
		// 		if (tmp == currentDate && !$('#day' + i).parent().hasClass('day-not-in-month')){
		// 			$('#day' + i).parent().addClass('month-view-today');
		// 			break;
		// 		}
		// 	}
		// });
	}

	// click button to view next or previous weeks in year in week view
	var url = window.location.pathname;
	url = url.substring(url.lastIndexOf('/') + 1);
	if (url === 'usercp'){
		$('.btn-group').on('click', '#btn-prevYear-weekView', function(){
			var tmp = $('#title-month').text();
			currentYear = parseInt(tmp);
			if(currentYear > 2015){
				document.getElementById('title-month').innerHTML = --currentYear;
				$('.week-view').empty();
				generateWeekView(currentYear);
			}
		});
		$('.btn-group').on('click', '#btn-nextYear-weekView', function(){
			var tmp = $('#title-month').text();
			currentYear = parseInt(tmp);
			document.getElementById('title-month').innerHTML = ++currentYear;
			$('.week-view').empty();
			generateWeekView(currentYear);
		});
	}

	// click button to view next or previous months in year in month view
	var url = window.location.pathname;
	url = url.substring(url.lastIndexOf('/') + 1);
	if (url === 'usercp'){
		$('.btn-group').on('click', '#btn-prevYear-monthView', function(){
			var tmp = $('#title-month').text();
			currentYear = parseInt(tmp);
			if(currentYear > 2015){
				document.getElementById('title-month').innerHTML = --currentYear;
			}
		});
		$('.btn-group').on('click', '#btn-nextYear-monthView', function(){
			var tmp = $('#title-month').text();
			currentYear = parseInt(tmp);
			document.getElementById('title-month').innerHTML = ++currentYear;
		});
	}

	// generate number of weeks to start and end weeks in create, edit goals
	var url = window.location.pathname;
	url = url.substring(url.lastIndexOf('/') + 1);
	if (url === 'creategoal' || url === 'editgoal'){
		// week start at 1
		$('#frequency').on('change', function(){
			if($('#frequency').val() === 'weekly'){
				$('#startWeek, #endWeek').text('');
				var numOfWeeksInYear = weeksInYear(parseInt($('#whichYearWeek').val())); 
				for (var i = 1; i <= numOfWeeksInYear; i++){
					$('#startWeek, #endWeek').append('<option value="' + i + '">Week ' + i +'</option>');
				}
			}
		});
		$('#whichYearWeek').on('change', function(){
			$('#startWeek, #endWeek').text('');
			var numOfWeeksInYear = weeksInYear(parseInt($('#whichYearWeek').val())); 
			for (var i = 1; i <= numOfWeeksInYear; i++){
				$('#startWeek, #endWeek').append('<option value="' + i + '">Week ' + i +'</option>');
			}	
		});
	}

	// generate years to start and end years in create goals
	var url = window.location.pathname;
	url = url.substring(url.lastIndexOf('/') + 1);
	if (url === 'creategoal' || url === 'editgoal'){
		currentYear = new Date().getFullYear();
		var limit = 2070;
		while(currentYear <= limit){
			$('#startYear, #endYear, #whichYearWeek, #whichYearMonth').append('<option value="' + currentYear + '">' + currentYear +'</option>');
			currentYear++;
		}
	}

	// click to choose frequency in create goal
	var url = window.location.pathname;
	url = url.substring(url.lastIndexOf('/') + 1);
	if (url === 'creategoal' || url === 'editgoal'){
		displayDayInterval();	
		$('#frequency, #repeat').on('change', function(){
			displayDayInterval();	
		});
	}

	// display start day when change frequency and repeat dropdown list
	function displayDayInterval(){
		switch($('#frequency').val()){
			case 'daily':
				switch($('#repeat').val()){
					case 'always':
						$('#weekly, #monthly, #yearly').slideUp(500);
						$('#daily').slideDown(500);
						$('#endDay').parents('.nopadding').addClass('hide-element');
						$('#goalStartTime, #goalEndTime').parents('.nopadding').removeClass('hide-element');
						break;
					case 'until':
						$('#weekly, #monthly, #yearly').slideUp(500);
						$('#daily').slideDown(500);
						$('#endDay').val($('#startDay').val()).parents('.nopadding').removeClass('hide-element');
						$('#goalStartTime, #goalEndTime').parents('.nopadding').removeClass('hide-element');
						break;
				}
				break;

			case 'weekly':
				switch($('#repeat').val()){
					case 'always':
						$('#daily, #monthly, #yearly').slideUp(500);
						$('#weekly').slideDown(500);
						$('#endWeek').parents('.nopadding').addClass('hide-element');
						$('#goalStartTime, #goalEndTime').parents('.nopadding').addClass('hide-element');
						break;
					case 'until':
						$('#daily, #monthly, #yearly').slideUp(500);
						$('#weekly').slideDown(500);
						$('#endWeek').val($('#startWeek').val()).parents('.nopadding').removeClass('hide-element');
						$('#goalStartTime, #goalEndTime').parents('.nopadding').addClass('hide-element');
						break;
				}
				break;

			case 'monthly':
				switch($('#repeat').val()){
					case 'always':
						$('#daily, #weekly, #yearly').slideUp(500);
						$('#monthly').slideDown(500);
						$('#endMonth, #goalStartTime, #goalEndTime').parents('.nopadding').addClass('hide-element');
						$('#goalStartTime, #goalEndTime').parents('.nopadding').addClass('hide-element');
						break;
					case 'until':
						$('#daily, #weekly, #yearly').slideUp(500);
						$('#monthly').slideDown(500);
						$('#endMonth').val($('#startMonth').val()).parents('.nopadding').removeClass('hide-element');
						$('#goalStartTime, #goalEndTime').parents('.nopadding').addClass('hide-element');
						break;
				}
				break;

			case 'yearly':
				switch($('#repeat').val()){
					case 'always':
						$('#daily, #weekly, #monthly').slideUp(500);
						$('#yearly').slideDown(500);
						$('#endYear, #goalStartTime, #goalEndTime').parents('.nopadding').addClass('hide-element');
						break;
					case 'until':
						$('#daily, #weekly, #monthly').slideUp(500);
						$('#yearly').slideDown(500);
						$('#endYear').val($('#startYear').val()).parents('.nopadding').removeClass('hide-element');
						$('#goalStartTime, #goalEndTime').parents('.nopadding').addClass('hide-element');
						break;
				}
				break;
		}
	}

	// set default value for create goal form in daily view
	var url = window.location.pathname;
	url = url.substring(url.lastIndexOf('/') + 1);
	if (url === 'creategoal'){
		var d = new Date();
		var tmp = d.getMonth() + 1;
		if(tmp < 10){
			tmp = '0' + tmp;
		}
		$('#startDay').val(tmp + '/' + d.getDate() + '/' + d.getFullYear());
	}

	// Form validation for create goals
	var url = window.location.pathname;
	url = url.substring(url.lastIndexOf('/') + 1);
	if (url === 'creategoal' || url === 'editgoal'){
		var d = new Date();
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth() + 1; //January is 0!
		var yyyy = today.getFullYear();
		$('#frequency, #repeat').on('change', function(){
			// set default value for weekly view
			if($('#frequency').val() === 'weekly'){
				$('#startWeek').val(getWeekNumber(d)[1]);
			}
			// set default value for monthly view
			if($('#frequency').val() === 'monthly'){
				$('#startMonth').val(d.getMonth() + 1);
			}

			if ($('#repeat').val() === 'always'){
				var today = new Date();
				var dd = today.getDate();
				var mm = today.getMonth() + 1; //January is 0!
				var yyyy = today.getFullYear();
				$('#startDay').on('change', function(){
					if (dd < 10) {
					    dd = '0' + dd;
					} 

					if (mm < 10) {
					    mm = '0' + mm;
					} 
					today = mm + '/' + dd + '/' + yyyy;
					var tmp = $('#startDay').val();
					if (Date.parse(today) > Date.parse(tmp)){
						$('#startDay').val(today);
					}
				});

				$('#startWeek, #endWeek').on('change', function(){
					var thisWeek = getWeekNumber(today)[1];
					var thisYear = yyyy;
					var tmp = $('#startWeek').val();
					var chosenWeek = tmp.substring(tmp.indexOf(' '), tmp.length);
					var chosenYear = parseInt($('#whichYearWeek').val());
					if (chosenWeek < thisWeek && chosenYear === thisYear) {
						$('#startWeek').val(thisWeek);
					}
				});	

				$('#startMonth, #endMonth').on('change', function(){
					var chosenMonth = $('#startMonth').val();
					var chosenYear = parseInt($('#whichYearMonth').val());
					var thisMonth = mm;
					var thisYear = yyyy;
					if (chosenMonth < thisMonth && chosenYear === thisYear) {
						$('#startMonth').val(thisMonth);
					}
				});
			}
			if ($('#repeat').val() === "until"){
				dayBiggerChecker();
				weekBiggerChecker();
				monthBiggerChecker();
				yearBiggerChecker();
				$('#startDay, #endDay').on('change', function(){
					var today = new Date();
					var dd = today.getDate();
					var mm = today.getMonth() + 1; //January is 0!
					var yyyy = today.getFullYear();
					if (dd < 10) {
					    dd = '0' + dd;
					} 

					if (mm < 10) {
					    mm = '0' + mm;
					} 
					today = mm + '/' + dd + '/' + yyyy;
					var tmp = $('#startDay').val();
					if (Date.parse(today) > Date.parse(tmp)){
						$('#startDay').val('');
						$('#startDay').val(today);
					}
					if($('#repeat').val() == "until"){ console.log($('#repeat').val());
						dayBiggerChecker();
					}
				});
				$('#startWeek, #endWeek').on('change', function(){
					var thisWeek = getWeekNumber(today)[1];
					var thisYear = yyyy;
					var tmp = $('#startWeek').val();
					var chosenWeek = tmp.substring(tmp.indexOf(' '), tmp.length);
					var chosenYear = parseInt($('#whichYearWeek').val());
					if (chosenWeek < thisWeek && chosenYear === thisYear) {
						$('#startWeek').val(thisWeek);
					}
					if($('#repeat').val() === "until"){
						weekBiggerChecker();
					}
				});
				$('#startMonth, #endMonth').on('change', function(){
					var chosenMonth = $('#startMonth').val();
					var chosenYear = parseInt($('#whichYearMonth').val());
					var thisMonth = mm;
					var thisYear = yyyy;
					if (chosenMonth < thisMonth && chosenYear === thisYear) {
						$('#startMonth').val(thisMonth);
					}
					if($('#repeat').val() === "until"){
						monthBiggerChecker();
					}
				});
				$('#startYear, #endYear').on('change', function(){
					if($('#repeat').val() === "until"){
						yearBiggerChecker();
					}
				});
			}
		});
		$('#goalStartTime, #goalEndTime').on('change', function(){
			timeBiggerChecker();
		});
	}

	// functions to check end day bigger than start day
	function dayBiggerChecker(){
		if(Date.parse($('#startDay').val()) > Date.parse($('#endDay').val())){
			$('#endDay').val($('#startDay').val());
		}
	}

	// functions to check end week bigger than start week
	function weekBiggerChecker(){
		if(parseInt($('#startWeek').val()) > parseInt($('#endWeek').val())){
			$('#endWeek').val($('#startWeek').val());
		}	
	}

	// functions to check end month bigger than start month
	function monthBiggerChecker(){
		if(parseInt($('#startMonth').val()) > parseInt($('#endMonth').val())){
			$('#endMonth').val($('#startMonth').val());
		}	
	}

	// functions to check end year bigger than start year
	function yearBiggerChecker(){
		if(parseInt($('#startYear').val()) > parseInt($('#endYear').val())){
			$('#endYear').val($('#startYear').val());
		}
	}

	// functions to check end time bigger than start time
	function timeBiggerChecker(){
		if($('#goalStartTime').val() > $('#goalEndTime').val()){
			$('#goalEndTime').val($('#goalStartTime').val());
		} 
	}

	// display value in editgoal
	var url = window.location.pathname;
	url = url.substring(url.lastIndexOf('/') + 1);
	if (url === 'editgoal'){
		if($('#frequency').val() === 'weekly'){
			var numOfWeeksInYear = weeksInYear(parseInt($('#whichYearWeek').val())); 
			for (var i = 1; i <= numOfWeeksInYear; i++){
				$('#startWeek, #endWeek').append('<option value="' + i + '">Week ' + i +'</option>');
			}
			$('#whichYearWeek option[value=' + YEAR + ']').attr("selected", "selected");
			$('#startWeek option[value=' + START + ']').attr("selected", "selected");
			$('#endWeek option[value=' + END + ']').attr("selected", "selected");
		}
		if($('#frequency').val() === 'monthly'){
			$('#whichYearMonth option[value=' + YEAR + ']').attr("selected", "selected");
		}
		if($('#frequency').val() === 'yearly'){
			$('#startYear option[value=' + START + ']').attr("selected", "selected");
			$('#endYear option[value=' + END + ']').attr("selected", "selected");
		}
	}

	// display val 0f entries dropdown in AdminCP.php
	$(".dropdown-menu li a").click(function(){
		$(this).parents(".btn-group").find('.selection').text($(this).text() + ' ');
  		$(this).parents(".btn-group").find('.selection').val($(this).text());
	});

	var url = window.location.pathname;
	url = url.substring(url.lastIndexOf('/') + 1);
	if (url === ''){
		// Validation in Sign-up
		$('#form-signup').formValidation({
	        message: 'This value is not valid',
	        icon: {
	            valid: 'glyphicon glyphicon-ok',
	            invalid: 'glyphicon glyphicon-remove',
	            validating: 'glyphicon glyphicon-refresh'
	        },
	        addOns: {
		        reCaptcha2: {
		            element: 'captchaContainer',
		            language: 'en',
		            theme: 'light',
		            siteKey: '6LerXAoTAAAAAMH23E2dvouyD8xQAg9xodq08Jkx',
		            timeout: 120,
		            message: 'The captcha is not valid'
			    }
		    },
	        fields: {
	        	email: {
	                validators: {
	                    notEmpty: {
	                        message: 'The email address is required and can\'t be empty'
	                    },
	                    emailAddress: {
	                        message: 'The input is not a valid email address'
	                    },
	                    stringLength: {
	                        max: 255,
	                        message: 'The email must be less than 255 characters long'
	                    }
	                }
	            },
	            password: {
	            	message: 'The password is not valid',
	                validators: {
	                    notEmpty: {
	                        message: 'The password is required and cannot be empty'
	                    },
	                    stringLength: {
	                        min: 6,
	                        max: 255,
	                        message: 'The password must be more than 6 and less than 255 characters long'
	                    },
	                    regexp: {
	                        regexp: /^[a-zA-Z0-9\ ]+$/,
	                        message: 'The password can only consist of alphabetical and number'
	                    }
	                }
	            },
	            password_confirmation: {
	            	validators: {
	            		notEmpty: {
	            			message: 'The comfirm password is required and can\'t be empty'
	            		},
	            		identical: {
	            			field: 'password',
	            			message: 'The password and its confirm are not the same'
	            		}
	            	}
	            }
	        }
	    }).on('success.field.fv', function(e, data) {
            // $(e.target)  --> The field element
            // data.fv      --> The FormValidation instance
            // data.field   --> The field name
            // data.element --> The field element

            var $parent = data.element.parents('.form-group');

            // Remove the has-success class
            $parent.removeClass('has-success');

            // Hide the success icon
            data.element.data('fv.icon').hide();
        });
	    // Validation in Log-in
		$('#form-login').formValidation({
	        message: 'This value is not valid',
	        icon: {
	            valid: 'glyphicon glyphicon-ok',
	            invalid: 'glyphicon glyphicon-remove',
	            validating: 'glyphicon glyphicon-refresh'
	        },
	        fields: {
	        	email: {
	                validators: {
	                    notEmpty: {
	                        message: 'The email address is required and can\'t be empty'
	                    },
	                    emailAddress: {
	                        message: 'The input is not a valid email address'
	                    },
	                    stringLength: {
	                        max: 255,
	                        message: 'The email must be less than 255 characters long'
	                    }
	                }
	            },
	            password: {
	            	message: 'The password is not valid',
	                validators: {
	                    notEmpty: {
	                        message: 'The password is required and cannot be empty'
	                    },
	                    stringLength: {
	                        min: 6,
	                        max: 255,
	                        message: 'The password must be more than 6 and less than 255 characters long'
	                    },
	                    regexp: {
	                        regexp: /^[a-zA-Z0-9\ ]+$/,
	                        message: 'The password can only consist of alphabetical and number'
	                    }
	                }
	            }
	        }
	    }).on('success.field.fv', function(e, data) {
            // $(e.target)  --> The field element
            // data.fv      --> The FormValidation instance
            // data.field   --> The field name
            // data.element --> The field element
            var $parent = data.element.parents('.form-group');

            // Remove the has-success class
            $parent.removeClass('has-success');
            // Hide the success icon
            data.element.data('fv.icon').hide();
    	});
	    // Validation in form-SendEmail
		$('#form-sendEmail').formValidation({
	        message: 'This value is not valid',
	        icon: {
	            valid: 'glyphicon glyphicon-ok',
	            invalid: 'glyphicon glyphicon-remove',
	            validating: 'glyphicon glyphicon-refresh'
	        },
	        fields: {
	        	email: {
	                validators: {
	                    notEmpty: {
	                        message: 'The email address is required and can\'t be empty'
	                    },
	                    emailAddress: {
	                        message: 'The input is not a valid email address'
	                    },
	                    stringLength: {
	                        max: 255,
	                        message: 'The email must be less than 255 characters long'
	                    }
	                }
	            }
	        }
	    }).on('success.field.fv', function(e, data) {
            // $(e.target)  --> The field element
            // data.fv      --> The FormValidation instance
            // data.field   --> The field name
            // data.element --> The field element

            var $parent = data.element.parents('.form-group');

            // Remove the has-success class
            $parent.removeClass('has-success');

            // Hide the success icon
            data.element.data('fv.icon').hide();
        });
	}

	// Validation in form-Profile
	var url = window.location.pathname;
	url = url.substring(url.lastIndexOf('/') + 1);
	if (url === 'profile'){
		// Validation in Profile
		$('#form-profile').formValidation({
	        message: 'This value is not valid',
	        icon: {
	            valid: 'glyphicon glyphicon-ok',
	            invalid: 'glyphicon glyphicon-remove',
	            validating: 'glyphicon glyphicon-refresh'
	        },
	        addOns: {
		        reCaptcha2: {
		            element: 'captchaContainer',
		            language: 'en',
		            theme: 'light',
		            siteKey: '6LerXAoTAAAAAMH23E2dvouyD8xQAg9xodq08Jkx',
		            timeout: 120,
		            message: 'The captcha is not valid'
			    }
		    },
	        fields: {
	            profileName: {
	                message: 'The username is not valid',
	                validators: {
	                    stringLength: {
	                        min: 6,
	                        max: 50,
	                        message: 'The username must be more than 6 and less than 50 characters long'
	                    },
	                    regexp: {
	                        regexp: /^[a-zA-Z0-9\ ]+$/,
	                        message: 'The username can only consist of alphabetical and number'
	                    }
	                }
	            },
	            profileFacebook:{
	            	message: 'The facebook is not valid',
	                validators: {
	                    stringLength: {
	                        max: 255,
	                        message: 'The facebook must be less than 255 characters long'
	                    }
	                }
	            },
	            profileTwitter:{
	            	message: 'The twitter is not valid',
	                validators: {
	                    stringLength: {
	                        max: 255,
	                        message: 'The facebook must be less than 255 characters long'
	                    }
	                }
	            },
	            profileGooglePlus:{
	            	message: 'The googleplus is not valid',
	                validators: {
	                    stringLength: {
	                        max: 255,
	                        message: 'The facebook must be less than 255 characters long'
	                    }
	                }
	            }, 
	            password: {
	            	validators: {
	            		stringLength: {
	            			min: 6,
	                        max: 255,
	                        message: 'The password must be more than 6 and less than 255 characters long'
	                    },
	                    regexp: {
	                        regexp: /^[a-zA-Z0-9]+$/,
	                        message: 'The password can only consist of alphabetical and number'
	                    }
	            	}
	            },
	            password_confirmation: {
	            	validators: {
	            		identical: {
	            			field: 'password',
	            			message: 'The password and its confirm are not the same'
	            		}
	            	}
	            },
	            oldPassword: {
	            	message: 'The password is not valid',
	                validators: {
	                    notEmpty: {
	                        message: 'The password is required and cannot be empty'
	                    },
	                    stringLength: {
	                        min: 6,
	                        max: 255,
	                        message: 'The password must be more than 6 and less than 255 characters long'
	                    },
	                    regexp: {
	                        regexp: /^[a-zA-Z0-9\ ]+$/,
	                        message: 'The password can only consist of alphabetical and number'
	                    }
	                }
	            }
	        }
	    }).on('success.field.fv', function(e, data) {
            // $(e.target)  --> The field element
            // data.fv      --> The FormValidation instance
            // data.field   --> The field name
            // data.element --> The field element

            var $parent = data.element.parents('.form-group');

            // Remove the has-success class
            $parent.removeClass('has-success');

            // Hide the success icon
            data.element.data('fv.icon').hide();
        });
	}
	
	//validation in create goal
	var url = window.location.pathname;
	url = url.substring(url.lastIndexOf('/') + 1);
	if (url === 'creategoal'){
		$('#create-goal').formValidation({
	        message: 'This value is not valid',
	        icon: {
	            valid: 'glyphicon glyphicon-ok',
	            invalid: 'glyphicon glyphicon-remove',
	            validating: 'glyphicon glyphicon-refresh'
	        },
	        fields: {
	        	goalTitle: {
	                validators: {
	                    notEmpty: {
	                        message: 'The goal\'s title is required and can\'t be empty'
	                    },
	                    stringLength: {
	                        min: 6,
	                        max: 255,
	                        message: 'The goal\'s title must be more than 6 and less than 255 characters long'
	                    }
	                }
	            }
	        }
	    }).on('success.field.fv', function(e, data) {
	            var $parent = data.element.parents('.form-group');

	            // Remove the has-success class
	            $parent.removeClass('has-success');

	            // Hide the success icon
	            data.element.data('fv.icon').hide();
	        });
	}

	//validation in contact us
	$('#form-contact').formValidation({
        message: 'This value is not valid',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
        	yourEmail: {
                validators: {
                    notEmpty: {
                        message: 'The email address is required and can\'t be empty'
                    },
                    emailAddress: {
                        message: 'The input is not a valid email address'
                    },
                    stringLength: {
                        max: 255,
                        message: 'The email must be more than 6 and less than 255 characters long'
                    }
                }
            },
        	subject: {
                validators: {
                    notEmpty: {
                        message: 'The subject is required and can\'t be empty'
                    },
                    stringLength: {
                        min: 6,
                        max: 255,
                        message: 'The subject must be more than 6 and less than 255 characters long'
                    }
                }
            },
            message: {
                validators: {
                    notEmpty: {
                        message: 'The message is required and can\'t be empty'
                    },
                    stringLength: {
                        min: 6,
                        message: 'The message must be more than 6 characters long'
                    }
                }
            }
        }
    }).on('success.field.fv', function(e, data) {
            var $parent = data.element.parents('.form-group');

            // Remove the has-success class
            $parent.removeClass('has-success');

            // Hide the success icon
            data.element.data('fv.icon').hide();
    });

    // Validation in reset form
	$('#form-reset').formValidation({
        message: 'This value is not valid',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
        	email: {
                validators: {
                    notEmpty: {
                        message: 'The email address is required and can\'t be empty'
                    },
                    emailAddress: {
                        message: 'The input is not a valid email address'
                    },
                    stringLength: {
                        max: 255,
                        message: 'The email must be more than 6 and less than 255 characters long'
                    }
                }
            },
            password: {
            	message: 'The password is not valid',
                validators: {
                    notEmpty: {
                        message: 'The password is required and cannot be empty'
                    },
                    stringLength: {
                        min: 6,
                        max: 255,
                        message: 'The password must be more than 6 and less than 255 characters long'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9\ ]+$/,
                        message: 'The password can only consist of alphabetical and number'
                    }
                }
            },
            password_confirmation: {
            	validators: {
            		notEmpty: {
            			message: 'The comfirm password is required and can\'t be empty'
            		},
            		identical: {
            			field: 'password',
            			message: 'The password and its confirm are not the same'
            		}
            	}
            }
        }
    }).on('success.field.fv', function(e, data) {
            var $parent = data.element.parents('.form-group');

            // Remove the has-success class
            $parent.removeClass('has-success');

            // Hide the success icon
            data.element.data('fv.icon').hide();
    });
	
	// Control form in Manage Users
	var url = window.location.pathname;
	url = url.substring(url.lastIndexOf('/') + 1);
	if (url === 'admincp'){
		//Control form in Manage Users
		$('#user-table').DataTable( {
	        "lengthMenu": [[-1, 25, 50, 100], ["All",25, 50, 100 ]]
	    } );

	   	$('#allcb').change(function(){
		    if($(this).prop('checked')){
		        $('tbody tr td input[type="checkbox"]').each(function(){
		            $(this).prop('checked', true);
		        });
		    }else{
		        $('tbody tr td input[type="checkbox"]').each(function(){
		            $(this).prop('checked', false);
		        });
		    }
		});

		$("#frmCheck").submit(function(){
		    var checked = $("#frmCheck input:checked").length > 0;
		    if (!checked){
		        alert("Please check at least one checkbox");
		        return false;
		    }
		});
   	}

   	var url = window.location.pathname;
	url = url.substring(url.lastIndexOf('/') + 1);
	if (url === 'feedback'){
		//Control form in Manage Users
		$('#fb-table').DataTable( {
	        "lengthMenu": [[-1, 25, 50, 100], ["All",25, 50, 100 ]]
	    } );

	    $('#allcb').change(function(){
		    if($(this).prop('checked')){
		        $('tbody tr td input[type="checkbox"]').each(function(){
		            $(this).prop('checked', true);
		        });
		    }else{
		        $('tbody tr td input[type="checkbox"]').each(function(){
		            $(this).prop('checked', false);
		        });
		    }
		});

		$("#frmCheck").submit(function(){
		    var checked = $("#frmCheck input:checked").length > 0;
		    if (!checked){
		        alert("Please check at least one checkbox");
		        return false;
		    }
		});
	}  
});
