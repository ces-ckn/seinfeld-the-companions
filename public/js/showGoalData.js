$(document).ready(function(){
	var monthArr = ['January','February','March','April','May','June','July','August','September','October','November','December'];
	var checkerWrapper = '<div id="checker-wrapper"><span id="checker" class="glyphicon-none"></span></div>';	// define checker-wrapper string for user to tick on their goals

	if (typeof(GOALS) !== 'undefined' && typeof(TICK) !== 'undefined'){
		var goalData = JSON.parse(GOALS);	// get data from JSON string
		var showTick = JSON.parse(TICK);	// get data from JSON string
		console.log('original');
		console.log(goalData);

		// reverse the goal data array so the first goal displayed will be the latest created goal
		goalData = goalData.reverse();

		// show goal's titles in the dropdown list
		for(var i = 0; i < goalData.length; i++){
			$('#goalList').append('<option value="">' + goalData[i].title + '</option>');
		}

		// show the first goal on the calendar and on the right sidebar
		var k = $('#goalList option:selected').index();
		$('#edit-goal, #del-goal').attr('disabled', false);
		showGoal(goalData, 0);
		
		switch(goalData[k].type){
			case 'daily':
				// show goal's detail on daily calendar
				showDailyDetail(k);
				
				// push daily ticks into json
			    $('.cal-day').on('click', function(){
	    	   		var tmp = $('#title-month').text();
					var currentMonth = monthArr.indexOf(tmp.substring(0, tmp.indexOf(' '))) + 1;
					if (parseInt(currentMonth) < 10){
						currentMonth = '0' + currentMonth;
					}
					var currentYear = tmp.substring(tmp.indexOf(' ') + 1, tmp.length);
					tmp = parseInt($(this).find('#checker-wrapper').siblings('.cal-date-number').text());
					if (tmp < 10){
						var currentDate = currentMonth + "/0" + $(this).find('#checker-wrapper').siblings('.cal-date-number').text() + "/" + currentYear;
					} else {
						var currentDate = currentMonth + "/" + $(this).find('#checker-wrapper').siblings('.cal-date-number').text() + "/" + currentYear;
					}
					var goalID = parseInt($('#goalid').text());
					var status = "";
					if ($(this).find('#checker').hasClass("glyphicon-ok")){
						status = "success";
		    		} else if ($(this).find('#checker').hasClass("glyphicon-remove")){
		    			status = "fail";
		    		} else if ($(this).find('#checker').hasClass("glyphicon-none")){
		    			status = "uncheck";
		    		}

					var dailyObj = {
				    	"goalId": goalID,
				        "items": currentDate,
				        "tick": status,
				        "type": "daily",
				        "start": goalData[k].start,
				        "end": goalData[k].end
				    };
				    showTick.push(dailyObj);
				});

				break;
			
			case 'weekly':
				// show goal's detail on weekly calendar
				showWeeklyDetail(k);

				// push weekly ticks into json
				$('.cal-week-cell').on('click', function(){
			        var tmp = $(this).find('#checker-wrapper').siblings('.cal-week-number').text();
			        var currentWeek = tmp.substring(tmp.indexOf(' '), tmp.length);
			        var goalID = parseInt($('#goalid').text());
					var status = "";
					if ($(this).find('#checker').hasClass("glyphicon-ok")){
						status = "success";
				    } else if ($(this).find('#checker').hasClass("glyphicon-remove")){
				        status = "fail";
				    } else if ($(this).find('#checker').hasClass("glyphicon-none")){
				        status = "uncheck";
				    }
				    
				    var weeklyObj = {
				    	"goalId": goalID,
				        "items": currentWeek,
				        "tick": status,
				        "type": "weekly",
				        "start": goalData[k].start,
				        "end": goalData[k].end
				    };
				    showTick.push(weeklyObj);
				});
				break;
			
			case 'monthly':
				// show goal's detail on monthly calendar
				showMonthlyDetail(k);

				// push monthly ticks into json
				$('.cal-month').on('click', function(){
			        var tmp = $(this).find('#checker-wrapper').siblings('.cal-month-name').text();
			        var currentMonth = monthArr.indexOf(tmp) + 1;
					var status = "";
					var goalID = parseInt($('#goalid').text());
					if ($(this).find('#checker').hasClass("glyphicon-ok")){
							status = "success";
			        } else if ($(this).find('#checker').hasClass("glyphicon-remove")){
			        		status = "fail";
			        } else if ($(this).find('#checker').hasClass("glyphicon-none")){
			        		status = "uncheck";
			        }
				    
				    var monthlyObj = {
				    	"goalId": goalID,
				        "items": currentMonth,
				        "tick": status,
				        "type": "monthly",
				        "start": goalData[k].start,
				        "end": goalData[k].end
				    }; 
				    showTick.push(monthlyObj);
				});
				break;

			case 'yearly':
				// show goal's detail on yearly calendar
				showYearlyDetail(k);

				// push yearly ticks into json
				$('.cal-year-cell').on('click', function(){
			        var currentYear = $(this).find('#checker-wrapper').siblings('.cal-year-number').text();
			        var goalID = parseInt($('#goalid').text());
					var status = "";
					if ($(this).find('#checker').hasClass("glyphicon-ok")){
						status = "success";
				    } else if ($(this).find('#checker').hasClass("glyphicon-remove")){
				        status = "fail";
				    } else if ($(this).find('#checker').hasClass("glyphicon-none")){
				        status = "uncheck";
				    }
				   	
				   	var yearlyObj = {
				    	"goalId": goalID,
				        "items": currentYear,
				        "tick": status,
				        "type": "yearly",
				        "start": goalData[k].start,
				        "end": goalData[k].end
				    };
				    showTick.push(yearlyObj);
				});
				break;
		}

		// catch event when change the dropdown list to view goal
		$('#goalList').on('change', function(){
			$('#checker-wrapper').remove();
			// show goal's detail on right sidebar
			$('#goal-year, #goal-time').empty();
			k = $('#goalList option:selected').index();
			showGoal(goalData, k);

			switch(goalData[k].type){
				case 'daily':
					// show goal's detail on daily calendar
					showDailyDetail(k);
					
					// push daily ticks into json
				    $('.cal-day').on('click', function(){
		    	   		var tmp = $('#title-month').text();
						var currentMonth = monthArr.indexOf(tmp.substring(0, tmp.indexOf(' '))) + 1;
						if (parseInt(currentMonth) < 10){
							currentMonth = '0' + currentMonth;
						}
						var currentYear = tmp.substring(tmp.indexOf(' ') + 1, tmp.length);
						tmp = parseInt($(this).find('#checker-wrapper').siblings('.cal-date-number').text());
						if (tmp < 10){
							var currentDate = currentMonth + "/0" + $(this).find('#checker-wrapper').siblings('.cal-date-number').text() + "/" + currentYear;
						} else {
							var currentDate = currentMonth + "/" + $(this).find('#checker-wrapper').siblings('.cal-date-number').text() + "/" + currentYear;
						}
						var goalID = parseInt($('#goalid').text());
						var status = "";
						if ($(this).find('#checker').hasClass("glyphicon-ok")){
							status = "success";
			    		} else if ($(this).find('#checker').hasClass("glyphicon-remove")){
			    			status = "fail";
			    		} else if ($(this).find('#checker').hasClass("glyphicon-none")){
			    			status = "uncheck";
			    		}

						var dailyObj = {
					    	"goalId": goalID,
					        "items": currentDate,
					        "tick": status,
					        "type": "daily",
					        "start": goalData[k].start,
					        "end": goalData[k].end
					    };
					    showTick.push(dailyObj);
					});

					break;
				
				case 'weekly':
					// show goal's detail on weekly calendar
					showWeeklyDetail(k);

					// push weekly ticks into json
					$('.cal-week-cell').on('click', function(){
				        var tmp = $(this).find('#checker-wrapper').siblings('.cal-week-number').text();
				        var currentWeek = tmp.substring(tmp.indexOf(' '), tmp.length);
				        var goalID = parseInt($('#goalid').text());
						var status = "";
						if ($(this).find('#checker').hasClass("glyphicon-ok")){
							status = "success";
					    } else if ($(this).find('#checker').hasClass("glyphicon-remove")){
					        status = "fail";
					    } else if ($(this).find('#checker').hasClass("glyphicon-none")){
					        status = "uncheck";
					    }
					    
					    var weeklyObj = {
					    	"goalId": goalID,
					        "items": currentWeek,
					        "tick": status,
					        "type": "weekly",
					        "start": goalData[k].start,
					        "end": goalData[k].end
					    };
					    showTick.push(weeklyObj);
					});
					break;
				
				case 'monthly':
					// show goal's detail on monthly calendar
					showMonthlyDetail(k);

					// push monthly ticks into json
					$('.cal-month').on('click', function(){
				        var tmp = $(this).find('#checker-wrapper').siblings('.cal-month-name').text();
				        var currentMonth = monthArr.indexOf(tmp) + 1;
						var status = "";
						var goalID = parseInt($('#goalid').text());
						if ($(this).find('#checker').hasClass("glyphicon-ok")){
								status = "success";
				        } else if ($(this).find('#checker').hasClass("glyphicon-remove")){
				        		status = "fail";
				        } else if ($(this).find('#checker').hasClass("glyphicon-none")){
				        		status = "uncheck";
				        }
					    
					    var monthlyObj = {
					    	"goalId": goalID,
					        "items": currentMonth,
					        "tick": status,
					        "type": "monthly",
					        "start": goalData[k].start,
					        "end": goalData[k].end
					    }; 
					    showTick.push(monthlyObj);
					});
					break;

				case 'yearly':
					// show goal's detail on yearly calendar
					showYearlyDetail(k);

					// push yearly ticks into json
					$('.cal-year-cell').on('click', function(){
				        var currentYear = $(this).find('#checker-wrapper').siblings('.cal-year-number').text();
				        var goalID = parseInt($('#goalid').text());
						var status = "";
						if ($(this).find('#checker').hasClass("glyphicon-ok")){
							status = "success";
					    } else if ($(this).find('#checker').hasClass("glyphicon-remove")){
					        status = "fail";
					    } else if ($(this).find('#checker').hasClass("glyphicon-none")){
					        status = "uncheck";
					    }
					   	
					   	var yearlyObj = {
					    	"goalId": goalID,
					        "items": currentYear,
					        "tick": status,
					        "type": "yearly",
					        "start": goalData[k].start,
					        "end": goalData[k].end
					    };
					    showTick.push(yearlyObj);
					});
					break;
			}
		});

		// catch event when user click on next (prev) button or next (prev) day in month of daily view
		$('.cal-month-box, .cal-header').on('click', '#btn-nextMonth-dayView, .day-in-next-month, #btn-prevMonth-dayView, .day-in-prev-month', function(){
			$('.cal-day #checker-wrapper').remove();
			k = $('#goalList option:selected').index();
			var startDate = parseInt(goalData[k].start.substring(3, 5));	// date format: mm/dd/yyyy
			var endDate = parseInt(goalData[k].end.substring(3, 5));		// ex: 08/19/2015
			var startMonth = parseInt(goalData[k].start.substring(0, 2));
			var endMonth = parseInt(goalData[k].end.substring(0, 2));
			var startYear = parseInt(goalData[k].start.substring(goalData[k].start.length - 4, goalData[k].start.length));
			var endYear = parseInt(goalData[k].end.substring(goalData[k].end.length - 4, goalData[k].end.length));
			var currentMonth = $('#title-month').text().substring(0, $('#title-month').text().indexOf(' '));
			var currentYear = parseInt($('#title-month').text().substring(
				$('#title-month').text().length - 4, 
				$('#title-month').text().length)
			);

			switch(goalData[k].repeat){
				case 'until':
					if (currentMonth === monthArr[startMonth - 1] && currentYear === startYear) {  
						if (startMonth === endMonth && startYear === endYear) {
							for (var i = 0; i < 42; i++){
								calStart = parseInt(document.getElementById('day' + i).innerHTML);
								if (calStart === startDate && !$('#day' + i).parent().hasClass('day-not-in-month')){
									calStart = i;
									break;
								}
							}

							for (var i = 0; i < 42; i++){
								calEnd = parseInt(document.getElementById('day' + i).innerHTML);
								if (calEnd === endDate && !$('#day' + i).parent().hasClass('day-not-in-month')){
									calEnd = i;
									break;
								}
							}
							for (var i = calStart; i <= calEnd; i++){
								$('#day' + i).after(checkerWrapper);
							}
						} else {
							printOnStartMonth(startDate);
						}
					} else if (isBetween(goalData[k].start, goalData[k].end, monthArr.indexOf(currentMonth) + 1, currentYear)) {
						for (var i = 0; i < 42; i++){
							$('#day' + i).after(checkerWrapper);
						}
					} else if (currentMonth === monthArr[endMonth - 1] && currentYear === endYear) {
						printOnEndMonth(endDate);
					}

					// remove checker on day not belong to current month
					$('.day-not-in-month').find('#checker-wrapper').remove();
					break;

				case 'always':
					if (currentMonth === monthArr[startMonth - 1] && currentYear === startYear) {  
						printOnStartMonth(startDate);
					} else {
						for (var i = 0; i < 42; i++){
							$('#day' + i).after(checkerWrapper);
						}
					}

					// remove checker on day not belong to current month
					$('.day-not-in-month').find('#checker-wrapper').remove();
					break;
			}

			showTickDaily(showTick, k);
		});
	
		// catch event when user click on next (prev) button or next (prev) day in month of weekly view
		$('.cal-month-box, .cal-header').on('click', '#btn-nextYear-weekView, #btn-prevYear-weekView', function(){
			$('.cal-week-cell #checker-wrapper').remove();
			k = $('#goalList option:selected').index();
			var whichYear = goalData[k].freqYear;
			var currentYear = parseInt($('#title-month').text());

			if (currentYear === whichYear) {
				printOnWeek(k);
			}

			showTickWeekly(showTick, k)
		});

		// catch event when user click on next (prev) button or next (prev) day in month of monthly view
		$('.cal-month-box, .cal-header').on('click', '#btn-nextYear-monthView, #btn-prevYear-monthView', function(){
			$('.cal-month #checker-wrapper').remove();
			k = $('#goalList option:selected').index();
			var whichYear = goalData[k].freqYear;
			var currentYear = parseInt($('#title-month').text());

			if (currentYear === whichYear) {
				printOnMonth(k);
			}

			showTickMonthly(showTick, k)
		});
	}

	// function to show goal's detail on daily calendar
	function showDailyDetail(k){
		$('#btn-prevYear-weekView, #btn-prevYear-monthView, #btn-prev-year-block').attr({
				'id': 'btn-prevMonth-dayView',
				'disabled': false
			});
		$('#btn-nextYear-weekView, #btn-nextYear-monthView, #btn-next-year-block').attr({
			'id': 'btn-nextMonth-dayView',
			'disabled': false
		});
		$(".week-view, .month-view, .year-view").slideUp(500);
		$(".day-view").slideDown(500);

		// get the time to display at the title above the calendar
		var dateStr = goalData[k].start;
		document.getElementById('title-month').innerHTML = 
			monthArr[parseInt(dateStr.substring(0, 2)) - 1] + ' ' + 
			dateStr.substring(dateStr.length - 4, dateStr.length);
		generateMonthView(
			parseInt(dateStr.substring(dateStr.length - 4, dateStr.length)),
			parseInt(dateStr.substring(0, 2))
		);

		switch(goalData[k].repeat){
			case 'until':
				var startDate = parseInt(goalData[k].start.substring(3, 5));
				var endDate = parseInt(goalData[k].end.substring(3, 5));
				var startMonth = parseInt(goalData[k].start.substring(0, 2)); 
				var endMonth = parseInt(goalData[k].end.substring(0, 2)); 
				var startYear = parseInt(goalData[k].start.substring(goalData[k].start.length - 4, goalData[k].start.length));
				var endYear = parseInt(goalData[k].end.substring(goalData[k].end.length - 4, goalData[k].end.length));
				var calStart, calEnd;
				
				$('.cal-day #checker-wrapper').remove();

				// handle display checker when end day not in current month
				if (startMonth === endMonth && startYear === endYear) {
					for (var i = 0; i < 42; i++){
						calStart = parseInt(document.getElementById('day' + i).innerHTML);
						if (calStart === startDate  && !$('#day' + i).parent().hasClass('day-not-in-month')){
							calStart = i;
							break;
						}
					}
					for (var i = 0; i < 42; i++){
						calEnd = parseInt(document.getElementById('day' + i).innerHTML);
						if (calEnd === endDate  && !$('#day' + i).parent().hasClass('day-not-in-month')){
							calEnd = i;
							break;
						}
					}
					for (var i = calStart; i <= calEnd; i++){
						$('#day' + i).after(checkerWrapper);
					}
				} else {
					printOnStartMonth(startDate);
				}

				// remove checker on day not belong to current month
				$('.day-not-in-month').find('#checker-wrapper').remove();
				break;

			case 'always':
				var startDate = parseInt(goalData[k].start.substring(3, 5));

				$('.cal-day #checker-wrapper').remove();
				printOnStartMonth(startDate);

				// remove checker on day not belong to current month
				$('.day-not-in-month').find('#checker-wrapper').remove();
				break;
		}

		showTickDaily(showTick, k);
	}

	// function to show tick on the daily view
	function showTickDaily(showTick, k){
		var calStart, calEnd;
		var startDate = parseInt(goalData[k].start.substring(3, 5));	// date format: mm/dd/yyyy
		var endDate = parseInt(goalData[k].end.substring(3, 5));		// ex: 08/19/2015
		var startMonth = parseInt(goalData[k].start.substring(0, 2));
		var endMonth = parseInt(goalData[k].end.substring(0, 2));
		var startYear = parseInt(goalData[k].start.substring(goalData[k].start.length - 4, goalData[k].start.length));
		var endYear = parseInt(goalData[k].end.substring(goalData[k].end.length - 4, goalData[k].end.length));
		var currentMonth = $('#title-month').text().substring(0, $('#title-month').text().indexOf(' '));
		var currentYear = parseInt($('#title-month').text().substring(
			$('#title-month').text().length - 4, 
			$('#title-month').text().length)
		);

		for (var t = 0; t < 42; t++){
			calStart = document.getElementById('day' + t).innerHTML;
			if (calStart == startDate && !$('#day' + t).parent().hasClass('day-not-in-month')){
				calStart = t;
				break;
			}
		}

		for (var t = 0; t < 42; t++){
			calEnd = document.getElementById('day' + t).innerHTML;
			if (calEnd == endDate  && !$('#day' + t).parent().hasClass('day-not-in-month')){
				calEnd = t;
				break;
			}
		}

		switch(goalData[k].repeat){
			case 'until':
				if (currentMonth === monthArr[startMonth - 1] && currentYear === startYear) {  
					if (startMonth === endMonth && startYear === endYear) {
						showDailyTick(showTick, calStart, calEnd, monthArr.indexOf(currentMonth) + 1, currentYear);
					} else {
						showDailyTick(showTick, calStart, 42, monthArr.indexOf(currentMonth) + 1, currentYear);
					}
				} else if (isBetween(goalData[k].start, goalData[k].end, monthArr.indexOf(currentMonth) + 1, currentYear)) {
					showDailyTick(showTick, 0, 42, monthArr.indexOf(currentMonth) + 1, currentYear);
				} else if (currentMonth === monthArr[endMonth - 1] && currentYear === endYear) {
					showDailyTick(showTick, 0, calEnd, monthArr.indexOf(currentMonth) + 1, currentYear);
				}

				break;

			case 'always':
				if (currentMonth === monthArr[startMonth - 1] && currentYear === startYear) {
					showDailyTick(showTick, calStart, 42, monthArr.indexOf(currentMonth) + 1, currentYear);
				} else {
					showDailyTick(showTick, 0, 42, monthArr.indexOf(currentMonth) + 1, currentYear);
				}
				break;
		}
	}

	// sub-function to display "check" on daily view
	function showDailyTick(showTick, calStart, calEnd, currentMonth, currentYear){
		var dbDate, dbDay;
		for (var i = 0; i < showTick.length; i++){
			if(parseInt($('#goalid').text()) == showTick[i].goalId){
				dbDate = showTick[i].items;
				dbDay = parseInt(dbDate.substring(3, 5));	// day from db
				dbMonth = parseInt(dbDate.substring(0, 2));
				dbYear = dbDate.substring(dbDate.length - 4, dbDate.length);

				if (dbYear == currentYear){
					if (dbMonth == currentMonth){
						for (var j = calStart; j <= calEnd; j++){
							if (dbDay == $('#day' + j).text()){
								if (showTick[i].tick == 'success'){	
									$('#day' + j).siblings().find('#checker').removeClass().addClass('glyphicon glyphicon-ok').css({
										'color': '#26C281',
										'font-size': '1.2em'
									});
									$('#day' + j).siblings().css('border-color', '#26C281');
								}
								if (showTick[i].tick == 'fail'){
									$('#day' + j).siblings().find('#checker').removeClass().addClass('glyphicon glyphicon-remove').css({
										'color': 'red',
										'font-size': '1.2em'
									});
									$('#day' + j).siblings().css('border-color', '#F15927');
								}
								if (showTick[i].tick == 'uncheck'){	
									$('#day' + j).siblings().find('#checker').removeClass().addClass('glyphicon glyphicon-none').css({
										'color': 'red',
										'font-size': '1.2em'
									});
									$('#day' + j).siblings().css('border-color', '#F15927');
								}
							}
						}
					}
				}
			}
		}
	}

	// function to show goal's detail on weekly calendar
	function showWeeklyDetail(k){
		$('#btn-prevMonth-dayView, #btn-prevYear-monthView, #btn-prev-year-block').attr({
			'id': 'btn-prevYear-weekView',
			'disabled': false
		});
		$('#btn-nextMonth-dayView, #btn-nextYear-monthView, #btn-next-year-block').attr({
			'id': 'btn-nextYear-weekView',
			'disabled': false
		});
		$(".day-view, .month-view, .year-view").slideUp(500);
		// $('#btn-today').attr('disabled', true);
		$(".week-view").slideDown(500);

		document.getElementById('title-month').innerHTML = goalData[k].freqYear;
		$('.week-view').empty();
		generateWeekView(goalData[k].freqYear);

		var whichYear = goalData[k].freqYear;
		var currentYear = parseInt($('#title-month').text());

		if (currentYear === whichYear) {
			printOnWeek(k);
		}

		showTickWeekly(showTick, k);
	}

	// function to show tick on the weekly view
	function showTickWeekly(showTick, k){
		var startWeek = parseInt(goalData[k].start);	// week format: number: 1, 2, 3, ...
		var endWeek = parseInt(goalData[k].end);

		switch(goalData[k].repeat){
			case 'until':
				showWeeklyTick(showTick, startWeek, endWeek);
				break;

			case 'always':
				showWeeklyTick(showTick, startWeek, 53);
				break;
		}
	}

	// sub-function to show tick on weekly view
	function showWeeklyTick(showTick, startWeek, endWeek){
		for (var i = 0; i < showTick.length; i++){
			var dbWeek = showTick[i].items;
			if(parseInt($('#goalid').text()) == showTick[i].goalId){
				for (var j = startWeek; j <= endWeek; j++){
					if (dbWeek == j){
						if (showTick[i].tick == 'success'){	
							$('#week' + j).siblings().find('#checker').removeClass().addClass('glyphicon glyphicon-ok').css({
								'color': '#26C281',
								'font-size': '1.2em'
							});
							$('#week' + j).siblings().css('border-color', '#26C281');
						}

						if (showTick[i].tick == 'fail'){
							$('#week' + j).siblings().find('#checker').removeClass().addClass('glyphicon glyphicon-remove').css({
								'color': 'red',
								'font-size': '1.2em'
							});
							$('#week' + j).siblings().css('border-color', '#F15927');
						}
						if (showTick[i].tick == 'uncheck'){	
							$('#week' + j).siblings().find('#checker').removeClass().addClass('glyphicon glyphicon-none').css({
								'color': 'red',
								'font-size': '1.2em'
							});
							$('#week' + j).siblings().css('border-color', '#F15927');
						}
					}
				}
			}
		}
	}

	// function to show goal's detail on monthly calendar
	function showMonthlyDetail(k){
		$('#btn-prevMonth-dayView, #btn-prevYear-weekView, #btn-prev-year-block').attr({
			'id': 'btn-prevYear-monthView',
			'disabled': false
		});
		$('#btn-nextMonth-dayView, #btn-nextYear-weekView, #btn-next-year-block').attr({
			'id': 'btn-nextYear-monthView',
			'disabled': false
		});
		$(".day-view, .week-view, .year-view").slideUp(500);
		// $('#btn-today').attr('disabled', true);
		$(".month-view").slideDown(500);

		var whichYear = goalData[k].freqYear;
		document.getElementById('title-month').innerHTML = whichYear;
		var currentYear = parseInt($('#title-month').text());
		$('.cal-month #checker-wrapper').remove();

		if (currentYear === whichYear) {
			printOnMonth(k);
		}

		showTickMonthly(showTick, k);
	}

	// function to show tick on the monthly view
	function showTickMonthly(showTick, k){
		var startMonth = parseInt(goalData[k].start);
		var endMonth = parseInt(goalData[k].end);

		switch(goalData[k].repeat){
			case 'until':
				showMonthlyTick(showTick, startMonth, endMonth);
				break;

			case 'always':
				showMonthlyTick(showTick, startMonth, 12);
				break;
		}
	}

	// sub-function to show tick on the monthly view
	function showMonthlyTick(showTick, startMonth, endMonth){
		for (var i = 0; i < showTick.length; i++){
			var dbMonth = showTick[i].items;
			if(parseInt($('#goalid').text()) == showTick[i].goalId){
				for (var j = startMonth; j <= endMonth; j++){
					if (dbMonth == j){
						if (showTick[i].tick == 'success'){	
							$('#month' + j).siblings().find('#checker').removeClass().addClass('glyphicon glyphicon-ok').css({
								'color': '#26C281',
								'font-size': '1.2em'
							});
							$('#month' + j).siblings().css('border-color', '#26C281');
						}

						if (showTick[i].tick == 'fail'){
							$('#month' + j).siblings().find('#checker').removeClass().addClass('glyphicon glyphicon-remove').css({
								'color': 'red',
								'font-size': '1.2em'
							});
							$('#month' + j).siblings().css('border-color', '#F15927');
						}
						if (showTick[i].tick == 'uncheck'){	
							$('#month' + j).siblings().find('#checker').removeClass().addClass('glyphicon glyphicon-none').css({
								'color': 'red',
								'font-size': '1.2em'
							});
							$('#month' + j).siblings().css('border-color', '#F15927');
						}
					}
				}
			}
		}
	}

	// function to show goal's detail on yearly calendar
	function showYearlyDetail(k){
		document.getElementById('title-month').innerHTML = $('.cal-year-cell').first().text() + ' - ' + $('.cal-year-cell').last().text();
		$('#btn-prevMonth-dayView, #btn-prevYear-weekView, #btn-prevYear-monthView').attr({
			'id': 'btn-prev-year-block',
			'disabled': true
		});
		$('#btn-nextMonth-dayView, #btn-nextYear-weekView, #btn-nextYear-monthView').attr({
			'id': 'btn-next-year-block',
			'disabled': true
		});
		$(".day-view, .week-view, .month-view").slideUp(500);
		// $('#btn-today').attr('disabled', true);
		$(".year-view").slideDown(500);

		var startYear = parseInt(goalData[k].start);
		var endYear = parseInt(goalData[k].end);

		$('.year-view').empty();
		generateYearBlock();

		switch(goalData[k].repeat){
			case 'until':
				for (var i = startYear; i <= endYear; i++){
					$('#year' + i).after(checkerWrapper);	
				}
				break;

			case 'always':
				for (var i = startYear; i <= 2070; i++){
					$('#year' + i).after(checkerWrapper);	
				}
				break;
		}

		showTickYearly(showTick, k)
	}

	// function to show tick on the yearly view
	function showTickYearly(showTick, k){
		var startYear = parseInt(goalData[k].start);
		var endYear = parseInt(goalData[k].end);

		switch(goalData[k].repeat){
			case 'until':
				showYearlyTick(showTick, startYear, endYear);
				break;

			case 'always':
				showYearlyTick(showTick, startYear, 2070);
				break;
		}
	}

	// sub-function to show tick on the yearly view
	function showYearlyTick(showTick, startYear, endYear){
		for (var i = 0; i < showTick.length; i++){
			var dbYear = showTick[i].items;
			if(parseInt($('#goalid').text()) == showTick[i].goalId){
				for (var j = startYear; j <= endYear; j++){
					if (dbYear == j){
						if (showTick[i].tick == 'success'){	
							$('#year' + j).siblings().find('#checker').removeClass().addClass('glyphicon glyphicon-ok').css({
								'color': '#26C281',
								'font-size': '1.2em'
							});
							$('#year' + j).siblings().css('border-color', '#26C281');
						}
						if (showTick[i].tick == 'fail'){
							$('#year' + j).siblings().find('#checker').removeClass().addClass('glyphicon glyphicon-remove').css({
								'color': 'red',
								'font-size': '1.2em'
							});
							$('#year' + j).siblings().css('border-color', '#F15927');
						}
						if (showTick[i].tick == 'uncheck'){	
							$('#year' + j).siblings().find('#checker').removeClass().addClass('glyphicon glyphicon-none').css({
								'color': 'red',
								'font-size': '1.2em'
							});
							$('#year' + j).siblings().css('border-color', '#F15927');
						}
					}
				}
			}
		}
	}

	// print checkable day in start month
	function printOnStartMonth(startDate){
		var calStart; 
		var tmp = false;
		for (var i = 0; i < 42; i++){ 
			calStart = parseInt(document.getElementById('day' + i).innerHTML);
			if (calStart === startDate && !$('#day' + i).parent().hasClass('day-not-in-month')){ 
				calStart = i;
				break;
			}
		}
		for (var i = calStart; i < 42; i++){
			$('#day' + i).after(checkerWrapper);
		}
	}

	// print checkable day in end month
	function printOnEndMonth(endDate) {
		var calEnd;
		for (var i = 0; i < 42; i++){
			calEnd = parseInt(document.getElementById('day' + i).innerHTML);
			if (calEnd === endDate  && !$('#day' + i).parent().hasClass('day-not-in-month')){
				calEnd = i; 
				break;
			}
		}
		for (var i = 0; i <= calEnd; i++){
			$('#day' + i).after(checkerWrapper);
		}
	}

	// print checkable day in week
	function printOnWeek(k){
		var whichYear = goalData[k].freqYear;
		var startWeek = parseInt(goalData[k].start);	// week format: number: 1, 2, 3, ...
		var endWeek = parseInt(goalData[k].end);
		var calStart, calEnd;

		switch(goalData[k].repeat){
			case 'until':
				// for (var i = 1; i <= weeksInYear(whichYear); i++){
				// 	if (i === startWeek){
				// 		calStart = i;
				// 		break;
				// 	}
				// } 
				// for (var i = calStart; i <= weeksInYear(whichYear); i++){
				// 	if (i === endWeek){
				// 		calEnd = i;
				// 		break;
				// 	}
				// }
				for (var i = startWeek; i <= endWeek; i++){
					$('#week' + i).after(checkerWrapper);
				}
				break;

			case 'always':
				// for (var i = 1; i < weeksInYear(whichYear); i++){
				// 	var tmp = document.getElementById('week' + i).innerHTML;
				// 	calStart = parseInt(tmp.substring(tmp.indexOf(' '), tmp.length));
				// 	if (calStart === startWeek){
				// 		calStart = i;
				// 		break;
				// 	}
				// }
				for (var i = startWeek; i <= weeksInYear(whichYear); i++){
					$('#week' + i).after(checkerWrapper);
				}
				break;
		}
	}

	// print checkable month in monthly view
	function printOnMonth(k){
		var whichYear = goalData[k].freqYear;
		var startMonth = parseInt(goalData[k].start);	// week format: number: 1, 2, 3, ...
		var endMonth = parseInt(goalData[k].end);
		var calStart, calEnd;
		
		switch(goalData[k].repeat){
			case 'until':
				for (var i = startMonth; i <= endMonth; i++) {
					$('#month' + i).after(checkerWrapper);
				}
				break;

			case 'always':
				for (var i = startMonth; i <= 12; i++) {
					$('#month' + i).after(checkerWrapper);
				}
				break;
		}
	}

	// check if current month is between start date and end date
	function isBetween(start, end, checkMonth, checkYear){
		var startMonth = parseInt(start.substring(0, 2));
		var endMonth = parseInt(end.substring(0, 2));
		var startYear = parseInt(start.substring(start.length - 4, start.length));
		var endYear = parseInt(end.substring(end.length - 4, end.length));

		if (endYear === startYear) {
			if (checkMonth > startMonth && checkMonth < endMonth) {
				return true;
			} else {
				return false;
			}
		} else {
			if (checkYear === startYear) {
				if (checkMonth > startMonth) {
					return true;
				} else {
					return false;
				}
			} else if (checkYear === endYear) {
				if (checkMonth < endMonth) {
					return true;
				} else {
					return false;
				}
			} else if (checkYear > startYear && checkYear < endYear) {
				return true;
			} else {
				return false;
			}
		}
	}

	// show goal's detail on the right sidebar
	function showGoal(goalData, index){
		document.getElementById('goalid').innerHTML = goalData[index].goalId;
		$('#goalid').attr('value', goalData[index].goalId);
		$('.modal-footer #goalid').attr('value', goalData[index].goalId);
		document.getElementById('goal-title').innerHTML = goalData[index].title;
		document.getElementById('goal-desc').innerHTML = goalData[index].description;
		document.getElementById('goal-frequency').innerHTML = goalData[index].type;
		
		if (goalData[index].type == 'daily'){
			if (goalData[index].startTime === goalData[index].endTime) {
				$('#goal-time').append('<strong>Time: </strong><span name="goal-time">' + goalData[index].startTime.substring(0, 5) + '</span><br>');
			} else {
				$('#goal-time').append('<strong>Time: </strong><span name="goal-time">' + goalData[index].startTime.substring(0, 5) + ' - ' + goalData[index].endTime.substring(0, 5) + '</span><br>');
			}
		}

		switch(goalData[index].repeat){
			case 'until':
				$('#goal-repeat').empty();
				if (goalData[index].type === 'weekly') {
					$('#goal-repeat').append(
						'Week ' + goalData[index].start + 
						' - ' + 
						'Week ' + goalData[index].end);
				} else if (goalData[index].type === 'monthly') {
					$('#goal-repeat').append(monthArr[goalData[index].start - 1] + ' - ' + monthArr[goalData[index].end - 1]);
				} else {
					$('#goal-repeat').append(goalData[index].start + ' - ' + goalData[index].end);
				}
				break;

			case 'always':
				document.getElementById('goal-repeat').innerHTML = 'Always, start at ';
				if (goalData[index].type === 'weekly') {
					$('#goal-repeat').append('Week ' + goalData[index].start);
				} else if (goalData[index].type === 'monthly') {
					$('#goal-repeat').append(monthArr[goalData[index].start - 1]);
				} else {
					$('#goal-repeat').append(goalData[index].start);
				}
				break;
		}

		if (goalData[index].freqYear > 0){
			$('#goal-year').append('<strong>Year: </strong><span name="goal-year">' + goalData[index].freqYear + '</span><br>');
		}

		if (goalData[index].remind === 1){
			document.getElementById('goal-remind').innerHTML = 'Yes';
		} else {
			document.getElementById('goal-remind').innerHTML = 'No';
		}

		if (goalData[index].autocheck === 1){
			document.getElementById('goal-autocheck').innerHTML = 'Yes';
		} else {
			document.getElementById('goal-autocheck').innerHTML = 'No';
		}
	}
});