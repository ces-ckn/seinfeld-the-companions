$(document).ready(function(){
	if (typeof(GOALS) !== 'undefined' && typeof(TICK) !== 'undefined'){
		var goalData = JSON.parse(GOALS);	// get data from JSON string
		var showTick = JSON.parse(TICK);	// get data from JSON string
		var goalIdList = [];		
		var longestGoal = []; 
		var countSuccess = 0, tmpCounter = 0;

		for (var i = 0; i < goalData.length; i++){
			goalIdList[i] = [goalData[i].goalId, goalData[i].title];
		}

		for (var i = 0; i < goalIdList.length; i++){
			for (var j = 0; j < showTick.length; j++){
				if (goalIdList[i][0] === showTick[j].goalId && showTick[j].tick === 'success'){
					tmpCounter++;
				}
			}
			if (tmpCounter > countSuccess){
				longestGoal.splice(0, longestGoal.length);
				countSuccess = tmpCounter;
				longestGoal.push(goalIdList[i][1]);		// [title]
				
			} else if (tmpCounter === countSuccess){
				countSuccess = tmpCounter;
				longestGoal.push(goalIdList[i][1]);
			}

			tmpCounter = 0;
		}
		
		// display the first child in array
		if (countSuccess === 0){
			document.getElementById('#longest-process').innerHTML = 'You don\'t have any goal in longest process';
		} else {
			var tmp = longestGoal[0];
			if (longestGoal.length > 1) {
				for (var i = 1; i < longestGoal.length - 1; i++){
					tmp = tmp + ', ' + longestGoal[i];
				}
				tmp = tmp + ', ' + longestGoal[longestGoal.length - 1];
			}
			document.getElementById('#longest-process').innerHTML = tmp + ' with ' + countSuccess + ' success(es)';
		}
	}
});