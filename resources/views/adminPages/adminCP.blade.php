@extends('master')

@section('title')
    X - Goals - AdminCP
@stop

@section('content')
	<!-- ===================== Admin's CP ===================== -->
	<section class="col-md-12">
		<div class="admincp-layout">
			<div class="col-md-12">
				<h1>User Management</h1>
			</div>
			<div class="col-md-12">
				<form class="form-inline admincp-control-button" id="frmCheck" method="POST" action="admincp">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">

					<button type="submit" class="btn btn-default btnSubmit" id="lockUser" name="lock" value="Lock">
						<span class="glyphicon glyphicon-lock"></span> Lock/Unlock
					</button>
					<button type="submit" class="btn btn-default btnSubmit" name="delete" value="Delete">
						<span class="glyphicon glyphicon-trash"></span> Delete
					</button>
				
					<table class="table table-responsive table-bordered table-hover" id="user-table">
						<thead>
							<tr>
								<th id="table-cell-center">
									<input type="checkbox" id="allcb" name="allcb">
								</th>
								<th id="table-cell-center">
									No.
								</th>
								<th>
									User's account
								</th>
								<th>
									User's name
								</th>
								
								<th>
									Role
								</th>
								<th>
									Status
								</th>
							</tr>
						</thead>
						<tbody>
							@foreach($users as $user)
							<tr>
								<td id="table-cell-center">
									@if($user->role == 0)
										<input type="checkbox" name="users[]" value="{{$user->userId}}">
									@endif
								</td>
								<td id="table-cell-center" >
									{{$num++}}
								</td>
								<td>
									{{$user->email}}
								</td>
								<td>
									{{$user->username}}
								</td>
								<td>
									@if($user->role)
										Admin
									@else
										User
									@endif
								</td>
								<td>
									@if($user->status)
										Active
									@else
										Locked
									@endif
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</form>
			</div>
		</div>
	</section>
@stop
@section('script')
	<script src="../public/datatable/js/jquery.dataTables.js"></script>
@stop
