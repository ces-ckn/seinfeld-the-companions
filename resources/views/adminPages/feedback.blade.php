@extends('master')

@section('title')
    X - Goals - Feedback
@stop

@section('content')
	<section class="col-md-12">
		<div class="admincp-layout">
			<div class="col-md-12">
				<h1>User's Feedback</h1>
			</div>
			<div class="col-md-12">
				<form class="form-inline admincp-control-button" id="frmCheck" method="POST" action="feedback">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					
					<button type="submit" class="btn btn-default btnSubmit" name="delete" value="Delete">
						<span class="glyphicon glyphicon-trash"></span> Delete
					</button>
					
					<table class="table table-responsive table-bordered table-hover" id="fb-table">
						<thead>
							<tr>
								<th id="table-cell-center">
									<input type="checkbox" id="allcb" name="allcb">
								</th>
								<th id="table-cell-center">
									No.
								</th>
								<th>
									Subject
								</th>
								<th>
									Email
								</th>
								<th>
									Message
								</th>
							</tr>
						</thead>
						<tbody>
							@foreach($fbs as $fb)
							<tr>
								<td id="table-cell-center">
									<input type="checkbox" name="fbs[]" value="{{$fb->fbId}}">
								</td>
								<td id="table-cell-center" >
									{{$num++}}
								</td>
								<td>
									{{$fb->subject}}
								</td>
								<td>
									{{$fb->email}}
								</td>
								<td>
									{{$fb->message}}
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</form>
			</div>
		</div>
	</section>
@stop

@section('script')
	<script src="../public/datatable/js/jquery.dataTables.js"></script>
@stop
