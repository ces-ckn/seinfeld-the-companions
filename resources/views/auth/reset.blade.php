@extends('master')

@section('title')
    X - Goals
@stop

@section('nav-elements')
  <li id="nav-element">
      <a id="manual-nav" href="#resetSection">RESET PASSWORD</a>
  </li>
@stop

@section('content')
    <!-- ===================== Carousel slide ===================== -->
    <section id="resetSection" class="col-md-12">
        <div class="carousel slide" id="carousel-116682">
            <ol class="carousel-indicators">
                <li class="active" data-slide-to="0" data-target="#carousel-116682">
                </li>
                <li data-slide-to="1" data-target="#carousel-116682">
                </li>
                <li data-slide-to="2" data-target="#carousel-116682">
                </li>
            </ol>
            <div class="carousel-inner">
                <div class="item active">
                    <img alt="Carousel Bootstrap First" src="/seinfeld-the-companions/public/img/slide1.jpg" />
                    <div class="carousel-caption">
                        <h4>
                                First Thumbnail label
                            </h4>
                    </div>
                </div>
                <div class="item">
                    <img alt="Carousel Bootstrap Second" src="/seinfeld-the-companions/public/img/slide2.jpg" />
                    <div class="carousel-caption">
                        <h4>
                                Second Thumbnail label
                            </h4>
                    </div>
                </div>
                <div class="item">
                    <img alt="Carousel Bootstrap Third" src="/seinfeld-the-companions/public/img/slide3.jpg" />
                    <div class="carousel-caption">
                        <h4>
                                Third Thumbnail label
                            </h4>
                    </div>
                </div>
            </div>
            <a class="left carousel-control" href="#carousel-116682" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="right carousel-control" href="#carousel-116682" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
        </div>
    </section>

    
    <!-- ===================== Reset Password ===================== -->
    <section id="aboutSection" class="col-md-12">
        <div class="about">
            <div class="row">
     <div class="col-md-8 col-md-offset-2">
       <div id="frmReset" class="panel panel-default">
         <div class="panel-heading" id="resetTitle">Reset Password</div>
         <div class="panel-body">
            @if (count($errors) > 0) 
                <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                  @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                  @endforeach
                   </ul>
                </div>
            @endif
          <form class="form-horizontal" role="form" id="form-reset" method="POST" action="password/reset">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="token" value="{{ $token }}">

            <div class="form-group">
              <label class="col-md-4 control-label">E-Mail Address</label>
              <div class="col-md-6">
                <div class="left-inner-addon">
                    <i class="glyphicon glyphicon-envelope"></i>
                </div>
                <input type="email" class="form-control" id="input" name="email" value="{{ old('email') }}">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-4 control-label">Password</label>
              <div class="col-md-6">
                <div class="left-inner-addon">
                    <i class="glyphicon glyphicon-asterisk"></i>
                </div>
                <input type="password" class="form-control" id="input" name="password">
              </div>
             </div>

             <div class="form-group">
               <label class="col-md-4 control-label">Confirm Password</label>
               <div class="col-md-6">
                <div class="left-inner-addon">
                    <i class="glyphicon glyphicon-asterisk"></i>
                </div>
                <input type="password" class="form-control" id="input" name="password_confirmation">
               </div>
              </div>

             <div class="form-group">
               <div class="col-md-6 col-md-offset-4">
                 <button type="submit" class="btn btn-default btn-lg btnSubmit btnSubmit-signup">Submit</button>
               </div>
             </div>
           </form>
         </div>
       </div>
     </div>
    </section>
@stop

@section('script')
    <script src="/seinfeld-the-companions/public/js/calendar.js"></script>
    <script src="/seinfeld-the-companions/public/dist/js/formValidation.js"></script>
    <script src="/seinfeld-the-companions/public/dist/js/framework/bootstrap.js"></script>
@stop  
