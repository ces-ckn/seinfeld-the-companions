@extends('master')

@section('title')
    X - Goals
@stop
 
@section('nav-elements')
    <li id="nav-element">
        <a id="manual-nav" href="#loginSection">LOG IN</a>
    </li>
    <li id="nav-element">
        <a id="manual-nav" href="#aboutSection">ABOUT US</a>
    </li>
    <li id="nav-element">
        <a id="manual-nav" href="#contactSection">CONTACT US</a>
    </li>
@stop

@section('content')
    <!-- ===================== Carousel slide ===================== -->
    <section id="homeSection" class="col-md-12">
        <div class="carousel slide" id="carousel-116682">
            <ol class="carousel-indicators">
                <li class="active" data-slide-to="0" data-target="#carousel-116682">
                </li>
                <li data-slide-to="1" data-target="#carousel-116682">
                </li>
                <li data-slide-to="2" data-target="#carousel-116682">
                </li>
            </ol>
            <div class="carousel-inner">
                <div class="item active">
                    <img alt="Carousel Bootstrap First" src="../public/img/slide1.jpg" />
                    <div class="carousel-caption">
                        <h4>
                                First Thumbnail label
                            </h4>
                    </div>
                </div>
                <div class="item">
                    <img alt="Carousel Bootstrap Second" src="../public/img/slide2.jpg" />
                    <div class="carousel-caption">
                        <h4>
                                Second Thumbnail label
                            </h4>
                    </div>
                </div>
                <div class="item">
                    <img alt="Carousel Bootstrap Third" src="../public/img/slide3.jpg" />
                    <div class="carousel-caption">
                        <h4>
                                Third Thumbnail label
                            </h4>
                    </div>
                </div>
            </div>
            <a class="left carousel-control" href="#carousel-116682" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="right carousel-control" href="#carousel-116682" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
        </div>
    </section>
    <!-- ===================== Login ===================== -->
    <section id="loginSection" class="col-md-12">
        <div class="text-center login">
            <button class="btn btn-danger btn-signup" >Sign up</button>
            <button class="btn btn-default btn-login">Log in</button>
            
            <div class="col-md-12" id="noti">
                <div class="col-md-8 col-md-offset-2">
                    @if(Session::has('fail'))
                        <div class="alert alert-danger">
                            {{ Session::get('fail') }}
                        </div>
                    @endif

                    @if (session('status'))
                      <div class="alert alert-success">
                             {{ session('status') }}
                      </div>
                    @endif

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
            
            <form class="form-horizontal" id="form-signup" name="form-signup" role="form" method="POST" action="register">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label class="col-sm-4 control-label">Email</label>
                    <div class="col-sm-5">
                        <div class="left-inner-addon">
                            <i class="glyphicon glyphicon-envelope"></i>
                        </div>
                        <input type="text" class="form-control" id="input" name="email" value="{{ old('email') }}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Password</label>
                    <div class="col-sm-5">
                        <div class="left-inner-addon">
                            <i class="glyphicon glyphicon-asterisk"></i>
                        </div>
                        <input type="password" class="form-control" id="input" name="password"> 
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Password again</label>
                    <div class="col-sm-5">
                        <div class="left-inner-addon">
                            <i class="glyphicon glyphicon-asterisk"></i>
                        </div>
                        <input type="password" class="form-control" id="input" name="password_confirmation">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Your name</label>
                    <div class="col-sm-5">
                        <div class="left-inner-addon">
                            <i class="glyphicon glyphicon-user"></i>
                        </div>
                        <input type="text" class="form-control" id="input" name="username" value="{{ old('username') }}">
                    </div>
                </div>

                <!-- <div class="form-group">
                    <label class="col-sm-4 control-label">Captcha</label>
                    <div class="col-sm-5">
                        <div id="captchaContainer"></div>
                    </div>
                </div> -->

                <div class="form-group">
                    <label class="col-sm-4 control-label"></label>
                    <div class="col-sm-5">
                        <button type="submit" class="btn btn-default btn-lg btnSubmit btnSubmit-signup">Sign up</button>
                    </div>
                </div>
            </form>

            <form name="form-login" class="form-horizontal" id="form-login" role="form" method="POST" action="login">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label class="col-sm-4 control-label">Email</label>
                    <div class="col-sm-5">
                        <div class="left-inner-addon">
                            <i class="glyphicon glyphicon-envelope"></i>
                        </div>
                        <input type="text" class="form-control" id="input" name="email" value="{{ old('email') }}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-4 control-label">Password</label>
                    <div class="col-sm-5">
                        <div class="left-inner-addon">
                            <i class="glyphicon glyphicon-asterisk"></i>
                        </div>
                        <input name="password" type="password" id="input" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-4">
                        <div class="checkbox">
                            <label id="checkbox-position">
                                <input name = "remember" type="checkbox"> Remember me
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label"></label>
                    <div class="col-sm-5">
                        <button type="submit" class="btn btn-default btn-lg btnSubmit btnSubmit-login" value="Log in">Log in</button>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label"></label>
                    <div class="col-sm-5">
                        <a id="forgotPassword" data-toggle="modal" data-target="#myModal">Forgot your password?</a>
                    </div>
                </div>
            </form>
            <!-- Send Email to User -->
            <form class="form-horizontal" id="form-sendEmail" role="form" method="POST" action="password/email">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div id="myModal" class="modal fade" role="dialog">
                    <div class="modal-dialog modal-lg">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Reset Your Password</h4>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Your email</label>
                                <div class="col-sm-5">
                                    <div class="left-inner-addon">
                                        <i class="glyphicon glyphicon-envelope"></i>
                                    </div>
                                    <input type="text" class="form-control" id="input" name="email" value="{{ old('email') }}">
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-danger btn-reset">Reset password</button>
                                <button type="button" class="btn btn-default btn-close" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>

    <!-- ===================== About us ===================== -->
    <section id="aboutSection" class="col-md-12">
        <div class="about">
            <div class="section-trigger">
                <h2 class="section-header">About us</h2>                
            </div>
            <button type="button" class="btn btn-default btn-down btnSubmit">
                <span class="glyphicon glyphicon-chevron-down arrow-down"></span>
            </button>
            <div class="section-content">
                <div class="col-md-4">
                    <img src="/seinfeld-the-companions/public/img/dev1.jpg" class="img-circle center-block about-img">
                    <p id="about-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eu congue urna. Phasellus placerat non mauris a vehicula. Vestibulum ut felis nec velit lacinia vulputate eget a justo. Phasellus fermentum lorem a bibendum viverra. Morbi porttitor euismod fringilla.</p>
                </div>
                <div class="col-md-4">
                    <img src="/seinfeld-the-companions/public/img/dev2.jpg" class="img-circle center-block about-img">
                    <p id="about-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eu congue urna. Phasellus placerat non mauris a vehicula. Vestibulum ut felis nec velit lacinia vulputate eget a justo. Phasellus fermentum lorem a bibendum viverra. Morbi porttitor euismod fringilla.</p>
                </div>
                <div class="col-md-4">
                    <img src="/seinfeld-the-companions/public/img/dev3.jpg" class="img-circle center-block about-img">
                    <p id="about-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eu congue urna. Phasellus placerat non mauris a vehicula. Vestibulum ut felis nec velit lacinia vulputate eget a justo. Phasellus fermentum lorem a bibendum viverra. Morbi porttitor euismod fringilla.</p>
                </div>
            </div>
        </div>
    </section>

    <!-- ===================== Contact ===================== -->
    <section id="contactSection" class="col-md-12"> 
        <div class="contact">
            <div class="section-trigger">
                <h2 class="section-header">Contact us</h2>                
            </div>
            <button type="button" class="btn btn-default btn-down btnSubmit">
                <span class="glyphicon glyphicon-chevron-down arrow-down"></span>
            </button>
            <div class="section-content">
                <form class="col-md-8 form-horizontal" id="form-contact" role="form" method="POST" action="contact">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group contact-form">
                        <label class="col-md-2 control-label">Subject</label>
                        <div class="col-md-8">
                            <div class="left-inner-addon">
                                <i class="glyphicon glyphicon-eye-open"></i> 
                            </div>
                            <input type="text" id="input" class="form-control" name="subject">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Your Email</label>
                        <div class="col-md-8">
                            <div class="left-inner-addon">
                                <i class="glyphicon glyphicon-envelope"></i>
                            </div>
                            <input type="email" class="form-control" id="input" name="yourEmail">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Message</label>
                        <div class="col-md-8">
                            <textarea class="form-control" name="message"></textarea> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label"></label>
                        <div class="col-md-5">
                            <button type="submit" class="btn btn-default btn-lg btnSubmit">Send</button>
                        </div>
                    </div>
                </form>
                <form class="col-md-4 form-horizontal" id="form-address">
                    <div class="tel">
                        <label>Tel:</label><p>1.800.705.1265</p>
                    </div>
                    <div class="email">
                        <label>Email:</label><p>Info@X-Goals.com</p>
                    </div>
                    <div class="adr">
                        <label>Adrress:</label><p>HighBridge Creative, Inc. 1490 Lafayette St. Suite 206 Denver, CO 80218</p>
                    </div>
                    <div class="icon">
                        <a href="#" class="facebook">Facebook</a>
                        <a href="#" class="twitter">Twiter</a>
                        <a href="#" class="google">Google</a>
                    </div>
                </form>
            </div>
        </div>
    </section>

@stop

@section('script')
    <script src="../public/js/calendar.js"></script>
    <script src="../public/dist/js/formValidation.js"></script>
    <script src="../public/dist/js/framework/bootstrap.js"></script>
    <script src="../public/dist/js/reCaptcha2.min.js"></script>
@stop  
