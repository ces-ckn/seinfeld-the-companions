<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> @yield('title') </title>

    <meta name="description" content="Create your own goals">
    <meta name="author" content="The Companions">

    <link href='http://fonts.googleapis.com/css?family=Comfortaa' rel='stylesheet'>

    
    <!-- <link href='http://fonts.googleapis.com/css?family=Comfortaa' rel='stylesheet'>
    <link href="../public/css/bootstrap.min.css" rel="stylesheet">
    <link href="../public/bootflat/css/bootflat.min.css" rel="stylesheet">
    <link href="../public/js/jqueryui/jquery-ui.min.css" rel="stylesheet">
    <link href="../public/dist/css/formValidation.css" rel="stylesheet"/>
    <link href="../public/datatable/css/jquery.dataTables.css" rel="stylesheet"/>
    <link href="../public/timepicker/jquery-timepicker.css" rel="stylesheet"/>
    <link href="../public/css/style.css" rel="stylesheet"> -->

    <link href='http://fonts.googleapis.com/css?family=Cabin|Signika' rel='stylesheet' type='text/css'>    
    <link href="/seinfeld-the-companions/public/css/bootstrap.min.css" rel="stylesheet">
    <link href="/seinfeld-the-companions/public/bootflat/css/bootflat.min.css" rel="stylesheet">
    <link href="/seinfeld-the-companions/public/js/jqueryui/jquery-ui.css" rel="stylesheet">
    <link href="/seinfeld-the-companions/public/dist/css/formValidation.css" rel="stylesheet"/>
    <link href="/seinfeld-the-companions/public/datatable/css/jquery.dataTables.css" rel="stylesheet"/>
    <link href="/seinfeld-the-companions/public/timepicker/jquery-timepicker.css" rel="stylesheet"/>
    <link href="/seinfeld-the-companions/public/css/style.css" rel="stylesheet">
</head>
<body>
    <header class="col-md-12">
        <!-- ===================== Navigation bar ===================== -->
        <nav class="navbar navbar-default navbar-inverse navbar-fixed-top" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul id="top-menu" class="nav navbar-nav">
                    @if(!Auth::check())
                        <li id="nav-element">
                            <a id="manual-nav" href="{{ URL::to('/') }}">HOME</a>
                        </li>
                    @else
                        <li id="nav-element">
                            <a id="manual-nav" href="{{ URL::to('/usercp') }}" >DASHBOARD</a>
                        </li>
                        <li id="nav-element">
                            <a id="manual-nav" href="{{ URL::to('/userrp') }}">REPORT</a>
                        </li>
                        <li id="nav-element">
                            <a id="manual-nav" href="{{ URL::to('/profile') }}">PROFILE</a>
                        </li>
                        @if(Auth::user()->role)
                        <li id="nav-element">
                            <a id="manual-nav" href="{{ URL::to('/admincp') }}">MANAGE USERS</a>
                        </li>
                        <li id="nav-element">
                            <a id="manual-nav" href="{{ URL::to('/feedback') }}">FEEDBACK</a>
                        </li>
                        @endif
                    @endif
                    
                    @yield('nav-elements')

                </ul>
                <ul id="top-menu" class="nav navbar-nav navbar-right">
                    @if(Auth::check())
                    <li id="nav-element" class="welcome">
                        <label>WELCOME, {{Auth::user()->email}}</label>
                    </li>
                    <li id="nav-element">
                        <a id="manual-nav" href="{{ URL::to('logout') }}">LOG OUT</a>
                    </li>
                    @endif
                </ul>
            </div>
        </nav>
    </header>

    
    @yield('content')
    <footer class="col-md-12 navbar-brand">
        <div class="copyright">
            Copyright © 2015 X-Goals design by The Companions
        </div>
    </footer>
    <a href="#" class="scrollToTop">Scroll To Top</a>

    <!--<script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script src="../public/js/jquery.min.js"></script>
    <script src="../public/js/bootstrap.min.js"></script>
    <!-- <script src="../public/js/jquery.smoothscroll.min.js"></script> -->
    <!-- <script src="../public/js/bootstrap-datepicker.min.js"></script> -->
    <!--<script src="../public/js/jqueryui/jquery-ui.min.js"></script>

    @yield('script')
    
    <script src="../public/js/scripts.js"></script>-->

    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script src="/seinfeld-the-companions/public/js/jquery.min.js"></script>
    <script src="/seinfeld-the-companions/public/js/bootstrap.min.js"></script>
    <!-- <script src="../public/js/jquery.smoothscroll.min.js"></script> -->
    <script src="/seinfeld-the-companions/public/dist/js/formValidation.js"></script>
    <script src="/seinfeld-the-companions/public/dist/js/framework/bootstrap.js"></script>
    <script src="/seinfeld-the-companions/public/js/jqueryui/jquery-ui.js"></script>
    
    @yield('script')
        
    <script src="/seinfeld-the-companions/public/js/scripts.js"></script>

    @yield('after-script')
</body>
</html>