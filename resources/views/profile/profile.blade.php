@extends('master')

@section('title')
    X - Goals - Profile
@stop
 
<!-- @section('nav-elements')
@stop -->

@section('content')
	<!-- ===================== Basic Information ===================== --> 
	<section class="col-md-12">
		<div class="profile-layout">
            <div class="col-md-12">
                <h1>Basic information</h1>
            </div>
            <div class="col-md-12">
                <div class="col-md-6">
                    @if(Session::has('success'))
                        <div class="alert alert-success">
                            {{ Session::get('success') }}
                        </div>
                    @endif

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
			<form class="form-horizontal" id="form-profile" role="form" method="POST" action="profile">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

	        	<div class="col-md-6 profile-layout-left">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Your email</label>
                        <div class="col-sm-5">
                            <div class="left-inner-addon">
                                <i class="glyphicon glyphicon-envelope"></i>
                            </div>
                            <input type="text" class="form-control" id="input" name="profileEmail" value="{{ Auth::user()->email }}" disabled>   
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Your name</label>
                        <div class="col-sm-5">
                            <div class="left-inner-addon">
                                <i class="glyphicon glyphicon-user"></i>
                            </div>
                            <input type="text" class="form-control" id="input" name="profileName" value="{{ Auth::user()->username }}" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Facebook</label>
                        <div class="col-sm-5">
                            <div class="left-inner-addon">
                                <i class="glyphicon glyphicon-star"></i>
                            </div>
                            <input type="text" class="form-control" id="input" name="profileFacebook" value="{{ Auth::user()->facebook }}"> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Twitter</label>
                        <div class="col-sm-5">
                            <div class="left-inner-addon">
                                <i class="glyphicon glyphicon-star"></i>
                            </div>
                            <input type="text" class="form-control" id="input" name="profileTwitter" value="{{ Auth::user()->twitter }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Google Plus</label>
                        <div class="col-sm-5">
                            <div class="left-inner-addon">
                                <i class="glyphicon glyphicon-star"></i>
                            </div>
                            <input type="text" class="form-control" id="input" name="profileGooglePlus" value="{{ Auth::user()->ggplus }}">   
                        </div>
                    </div>
		        </div>
	        	<div class="col-md-6 profile-layout-right">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">New password</label>
                        <div class="col-sm-5">
                            <div class="left-inner-addon">
                                <i class="glyphicon glyphicon-asterisk"></i>
                            </div>
                            <input type="password" class="form-control" id="input" name="password">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Confirm password</label>
                        <div class="col-sm-5">
                            <div class="left-inner-addon">
                                <i class="glyphicon glyphicon-asterisk"></i>
                            </div>
                            <input type="password" class="form-control" id="input" name="password_confirmation">
                        </div>
                    </div>
                	<div class="profile-wrapper">
	                    <div class="form-group">
	                        <label>Enter your current password to confirm changes:</label>
	                        <input type="password" class="form-control" name="oldPassword">
	                    </div>
	                    <div class="form-group">
	                        <button type="submit" class="btn btn-default btn-lg btnSubmit">Save changes</button>
	                    </div>
                    </div>
	        	</div>
	        </form>
        </div>
    </section>
@stop

@section('script')
    <script src="../public/js/calendar.js"></script>
    <script src="../public/dist/js/reCaptcha2.min.js"></script>
@stop  

