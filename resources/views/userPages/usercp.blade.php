@extends('master')

@section('title')
    X - Goals - Goal's Management
@stop
 
@section('meta')
 	<!-- <meta name="csrf_token" content="{{ csrf_token() }}" /> -->
@stop

@section('nav-elements')
    <li id="nav-element">
        <a id="manual-nav" href="#contactSection">CONTACT US</a>
    </li>
@stop

@section('content')
	<!-- ===================== User Control Panel ===================== -->
	<section class="col-md-12">
		<div class="usercp-layout">
			<div class="col-md-12">
				<h1>Goal's Management</h1>
			</div>
			
			<div class="col-md-12" id="noti">
                <div class="col-md-8 col-md-offset-2">
                    @if (session('status'))
                      <div class="alert alert-success">
                             {{ session('status') }}
                      </div>
                    @endif

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
			<!-- ===================== Begin Calendar ===================== -->
			<div class="col-md-9">
				<div id="calendar">
					<div class="cal-header form-inline">
						<select class="form-control" name="goalList" id="goalList">
							
						</select>
						<div class="btn-group">
							<button type="submit" class="btn btn-default" id="btn-prevMonth-dayView">
								<span class="glyphicon glyphicon-chevron-left"></span>
							</button>
							<button type="submit" class="btn btn-default" id="btn-today"></button>
							<button type="submit" class="btn btn-default" id="btn-nextMonth-dayView">
								<span class="glyphicon glyphicon-chevron-right"></span>
							</button>
						</div>
						<div class="btn-group">
							<a type="button" class="btn btn-danger" href="{{ URL::to('/creategoal') }}">NEW GOAL</a>
						</div>
						<h3 id="title-month">July 2015</h3>
					</div>

					<!-- ===================== Month view ===================== -->
					<div class="month-view">
						<div class="cal-4-month">
							<div class="cal-month">
								<div class="cal-month-name" id="month1">January</div>
								<!-- <div id='checker-wrapper'><span id="checker" class="glyphicon-none"></span></div> -->
							</div>
							<div class="cal-month">
								<div class="cal-month-name" id="month2">February</div>
								<!-- <div id='checker-wrapper'><span id="checker" class="glyphicon-none"></span></div> -->
							</div>
							<div class="cal-month">
								<div class="cal-month-name" id="month3">March</div>
								<!-- <div id='checker-wrapper'><span id="checker" class="glyphicon-none"></span></div> -->
							</div>
							<div class="cal-month">
								<div class="cal-month-name" id="month4">April</div>
								<!-- <div id='checker-wrapper'><span id="checker" class="glyphicon-none"></span></div> -->
							</div>
						</div>
						<div class="cal-4-month">
							<div class="cal-month">
								<div class="cal-month-name" id="month5">May</div>
								<!-- <div id='checker-wrapper'><span id="checker" class="glyphicon-none"></span></div> -->
							</div>
							<div class="cal-month">
								<div class="cal-month-name" id="month6">June</div>
								<!-- <div id='checker-wrapper'><span id="checker" class="glyphicon-none"></span></div> -->
							</div>
							<div class="cal-month">
								<div class="cal-month-name" id="month7">July</div>
								<!-- <div id='checker-wrapper'><span id="checker" class="glyphicon-none"></span></div> -->
							</div>
							<div class="cal-month">
								<div class="cal-month-name" id="month8">August</div>
								<!-- <div id='checker-wrapper'><span id="checker" class="glyphicon-none"></span></div> -->
							</div>
						</div>
						<div class="cal-4-month">
							<div class="cal-month">
								<div class="cal-month-name" id="month9">September</div>
								<!-- <div id='checker-wrapper'><span id="checker" class="glyphicon-none"></span></div> -->
							</div>
							<div class="cal-month">
								<div class="cal-month-name" id="month10">October</div>
								<!-- <div id='checker-wrapper'><span id="checker" class="glyphicon-none"></span></div> -->
							</div>
							<div class="cal-month">
								<div class="cal-month-name" id="month11">November</div>
								<!-- <div id='checker-wrapper'><span id="checker" class="glyphicon-none"></span></div> -->
							</div>
							<div class="cal-month">
								<div class="cal-month-name" id="month12">December</div>
								<!-- <div id='checker-wrapper'><span id="checker" class="glyphicon-none"></span></div> -->
							</div>
						</div>
					</div>

					<!-- ===================== Year view ===================== -->
					<div class="year-view">
						<!-- year's cells generated by scripts.js -->
					</div>

					<!-- ===================== Week view ===================== -->
					<div class="week-view">
						<!-- week's cells generated by scripts.js -->
					</div>

					<!-- ===================== Day view ===================== -->
					<div class="day-view">
						<div class="cal-row-head">
							<div class="cal-head">Sunday</div>
							<div class="cal-head">Monday</div>
							<div class="cal-head">Tueday</div>
							<div class="cal-head">Wednesday</div>
							<div class="cal-head">Thursday</div>
							<div class="cal-head">Friday</div>
							<div class="cal-head">Saturday</div>
						</div>
						<div class="cal-month-box">
							<!-- month's cells generated by scripts.js -->
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-3 goal-detail">
				<h3>Details</h3>
				<strong>Title: </strong><span id="goal-title" name='goal-title'></span>
				<br>
				<strong>Description: </strong><span id='goal-desc' name='goal-desc'></span>
				<br>
				<strong>Frequency: </strong><span id='goal-frequency' name='goal-frequency'></span>
				<br>
				<div id='goal-year'></div>
				<strong>Repeat: </strong><span id='goal-repeat' name='goal-repeat'></span>
				<br>
				<div id="goal-time"></div>
				<strong>Remind me: </strong><span id='goal-remind' name='goal-remind'></span>
				<br>
				<strong>Autocheck: </strong><span id='goal-autocheck' name='goal-autocheck'></span>
				<br>
				<form id="goal-detail" method="POST" action="editgoal">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input id="goalid" name="goalid"></input>
					<button type="submit" id="edit-goal" name="editgoal" value="editgoal" class="btn btn-default btnSubmit" disabled>Edit goal</button>
  					<button type="button" id="del-goal" class="btn btn-danger" data-toggle="modal" data-target="#notiModal" disabled>Delete goal</button>
				</form>

				<!-- ================== Modal for notification ================== -->
				<div class="modal fade" id="notiModal" role="dialog">
				    <div class="modal-dialog">
				        <div class="modal-content">
				            <div class="modal-header">
				                <button type="button" class="close" data-dismiss="modal">&times;</button>
				                <h4 class="modal-title">Warning</h4>
				            </div>
				            <div class="modal-body">
				                <p id="modal-content"></p>
				            </div>
				            <form class="modal-footer" method="POST" action="editgoal">
				            	<input type="hidden" name="_token" value="{{ csrf_token() }}">
				            	<input id="goalid" name="goalid"></input>
				                <button type="submit" id="del-goal" name="delgoal" value="delgoal" class="btn btn-danger">Delete</button>
				                <button type="button" class="btn btn-default btnSubmit" data-dismiss="modal">Cancel</button>
				            </form>
				        </div>
				    </div>
				</div>
			</div>
		</div>
	</section>

	<!-- ===================== Contact ===================== -->
    <section id="contactSection" class="col-md-12"> 
        <div class="contact">
            <div class="section-trigger">
                <h2 class="section-header">Contact us</h2>                
            </div>
            <button type="button" class="btn btn-default btn-down btnSubmit">
                <span class="glyphicon glyphicon-chevron-down arrow-down"></span>
            </button>
            <div class="section-content">
                <form class="col-md-8 form-horizontal" id="form-contact" method="POST" action="contact">
                	<input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group contact-form">
                        <label class="col-md-2 control-label">Subject</label>
                        <div class="col-md-8">
                            <div class="left-inner-addon">
                                <i class="glyphicon glyphicon-eye-open"></i> 
                            </div>
                            <input type="text" class="form-control" id="input" name="subject">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Your Email</label>
                        <div class="col-md-8">
                            <div class="left-inner-addon">
                                <i class="glyphicon glyphicon-envelope"></i>
                            </div>
                                <input type="email" class="form-control" id="input" name="yourEmail">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Message</label>
                        <div class="col-md-8">
                            <textarea class="form-control" name="message"></textarea> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label"></label>
                        <div class="col-md-5">
                            <button type="submit" class="btn btn-default btn-lg btnSubmit">Send</button>
                        </div>
                    </div>
                </form>
                <form class="col-md-4 form-horizontal" id="form-address">
                    <div class="tel">
                        <label>Tel:</label><p>1.800.705.1265</p>
                    </div>
                    <div class="email">
                        <label>Email:</label><p>Info@X-Goals.com</p>
                    </div>
                    <div class="adr">
                        <label>Adrress:</label><p>HighBridge Creative, Inc. 1490 Lafayette St. Suite 206 Denver, CO 80218</p>
                    </div>
                    <div class="icon">
                        <a href="#" class="facebook">Facebook</a>
                        <a href="#" class="twitter">Twiter</a>
                        <a href="#" class="google">Google</a>
                    </div>
                </form>
            </div>
        </div>
    </section>
@stop

@section('script')
	<script src="../public/js/calendar.js"></script>
	<script src="../public/js/calendarGenerator.js"></script> 
@stop

@section('after-script')
	<script>
		<?php if (isset($count)){ ?>
			<?php if($count != 0) { ?>
			window.GOALS = 	<?php echo json_encode($goal_data);  ?>;
			window.TICK = <?php echo json_encode($show_tick); } ?>;
		<?php } ?>
	</script>
	<script src="../public/js/showGoalData.js"></script>
@stop