@extends('master')

@section('title')
    X - Goals - User's Report
@stop
 
@section('content')
    <!-- ===================== Report ===================== -->
    <div class="col-md-12">
        <div class="report-layout">
            <div class="col-md-12">
                <h1>Report</h1>    
            </div>
            <div class="col-md-5 col-sm-12 col-xs-12">
                <table class="table table-responsive table-bordered table-hover" id="report-table">
                    <tr>
                        <td><b>Total Goals:</b></td>
                        <td>{{ $totalGoal }}</td>
                    </tr>
                    <tr>
                        <td><b>Success:</b></td>
                        <td>{{ $success }}</td>
                    </tr>
                    <tr>
                        <td><b>Fail:</b></td>
                        <td>{{ $fail }}</td>
                    </tr>
                    <tr>
                        <td><b>In progress:</b></td>
                        <td>{{ $inProgress }}</td>
                    </tr>
                    <tr>
                        <td><b>Total success:</b></td>
                        <td>{{ $totalSuccess }}%</td>
                    </tr>
                    <tr>
                        <td><b>Longest process:</b></td>
                        <td id='#longest-process'></td>
                    </tr>
                </table>
            </div>
            <div class="col-md-7 col-sm-12 col-xs-12" id="chart-part">
                <div class="col-md-6 col-sm-6 col-xs-12" id="pie-chart">
                    <canvas id="chart" width="400" height="400"></canvas>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12" >
                    <table id="chartData">
                        <tr style="color: #26C281">
                            <td>Success</td><td>{{ $success }}</td>
                        </tr>
                        <tr style="color: #F22613">
                            <td>Fail</td><td>{{ $fail }}</td>
                        </tr>
                        <tr style="color: #FFA400">
                            <td>In progress</td><td>{{ $inProgress }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    <script src="../public/js/chart.js"></script>
@stop

@section('after-script')
    <script>
        <?php if (isset($count)){ ?>
            <?php if($count != 0) { ?>
            window.GOALS =  <?php echo json_encode($goal_data);  ?>;
            window.TICK = <?php echo json_encode($show_tick); } ?>;
        <?php } ?>
    </script>
    <script src="../public/js/showReport.js"></script>
@stop